﻿using OpenCvSharp;
using OpenCvSharp.CPlusPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace OpenCvUtility.Track
{
    public class TrackingObject
    {
        public virtual double MAX_MOVING_SQUARE_SPEED { get; private set; } // max moving speed from previous frame in pixel/second
        public virtual double MAX_SIZE_DIFF_RATE { get; private set; } // has to be more than 1
        public virtual int UNDETECTED_LIMIT { get; private set; }

        public virtual Guid ID { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public DateTime DetectedTime;
        public List<DateTime> DetectedTimes = new List<DateTime>();
        public List<DateTime> UndetectedTimes = new List<DateTime>();
        public System.Windows.Rect BoundingRect { get; set; }

        public TrackingObject()
        {
            ID = Guid.NewGuid();
            DetectedTime = DateTime.Now;
            DetectedTimes.Add(DetectedTime);
        }

        public void AddDetectedTimes(TrackingObject oldObj)
        {
            if (oldObj.DetectedTimes == null || oldObj.DetectedTimes.Count == 0) throw new Exception("oldObj has to have tracked times");
            this.DetectedTimes = oldObj.DetectedTimes;
            this.DetectedTimes.Add(DetectedTime);
            // reset count of undetected
            this.UndetectedTimes.Clear();
        }

        public void AddUndetectedTimes()
        {
            this.UndetectedTimes.Add(DateTime.Now);
        }

        public bool IsLonglyUndetected()
        {
            return this.UndetectedTimes.Count > UNDETECTED_LIMIT;
        }

        public bool IsMovingOverSpeed(TrackingObject obj)
        {
            var ellapsedTime = Math.Abs((this.DetectedTime - obj.DetectedTime).TotalSeconds);
            return (MovingSquareDistance(obj) / ellapsedTime) > MAX_MOVING_SQUARE_SPEED;
        }

        public double MovingSquareDistance(TrackingObject obj)
        {
            return (this.X - obj.X) * (this.X - obj.X) + (this.Y - obj.Y) * (this.Y - obj.Y);
        }

        public bool IsSizeDifferent(TrackingObject obj)
        {
            var thisSize = this.BoundingRect.Width * this.BoundingRect.Height;
            var objSize = obj.BoundingRect.Width * obj.BoundingRect.Height;
            return (objSize > thisSize ? objSize / thisSize : thisSize / objSize) > MAX_SIZE_DIFF_RATE;
        }

        public Canvas DrawBoundingRect(Canvas canvas)
        {
            var random = new Random(ID.GetHashCode());
            var color = Color.FromRgb((byte)(200 * random.NextDouble() + 55), (byte)(200 * random.NextDouble() + 55), (byte)(200 * random.NextDouble() + 55));
            var rect = new Rectangle() { Height = BoundingRect.Height, Width = BoundingRect.Width, StrokeThickness = 5, Stroke = new SolidColorBrush(color) };
            rect.RenderTransform = new TranslateTransform(BoundingRect.X, BoundingRect.Y);
            canvas.Children.Add(rect);
            return canvas;
        }
    }

    public class SurfTrackingObject : TrackingObject
    {
        public virtual int WELL_MATCH_COUNT { get; private set; }

        public const int MIN_MATCH_COUNTS = 5;
        public static readonly CvSURFParams DEFAULT_PARAM = new CvSURFParams(800, true);

        //SURF_Points
        CvSURFPoint[] keypoints;
        float[][] descriptors;

        // 最もkeypointが多い状態を記録しておく
        CvSURFPoint[] bestKeypoints;
        float[][] bestDescriptors;

        public int NumKeypoints { get; private set; }

        // it shows whether this object matched many time so that it is reliable not to say this is a noise
        public bool IsWellMatched()
        {
            return DetectedTimes.Count > WELL_MATCH_COUNT;
        }

        public void CalcSurf(IplImage dishIpl, CvSURFParams param = null)
        {
            if (param == null) param = DEFAULT_PARAM;
            Cv2.InitModule_NonFree();
            Cv.ExtractSURF(dishIpl, null, out keypoints, out descriptors, param);
            NumKeypoints = keypoints.Length;
            if (bestKeypoints != null && bestDescriptors != null)
            {
                if (bestKeypoints.Length > keypoints.Length) return; // leave best keypoints
            }
            bestKeypoints = keypoints;
            bestDescriptors = descriptors;
            // Draw keypoints
            foreach (var kp in keypoints)
            {
                dishIpl.DrawCircle((int)kp.Pt.X, (int)kp.Pt.Y, (int)(kp.Size / 2.0), new CvScalar(255), 1);
            }
            //Cv.ShowImage(Guid.NewGuid().ToString(), dishIpl);
            //System.IO.Directory.CreateDirectory(@"C:\Users\共用アドレス\Desktop\detectedDishes\" + ID.ToString());
            //dishIpl.SaveImage(@"C:\Users\共用アドレス\Desktop\detectedDishes\" + DateTime.Now.ToString(Properties.Resources.DateTimeFormatText) + ".png");
        }

        public bool IsSameObject(SurfTrackingObject newObj)
        {
            //if (IsMovingOverSpeed(newObj))
            //{
            //    Console.WriteLine("Moving too fast!");
            //    return false;
            //}
            //if (IsSizeDifferent(newObj)) return false;

            var keyPairs = FindPairs(this.keypoints, this.descriptors, newObj.keypoints, newObj.descriptors);
            // keyPairs.Length is number of pairs
            if (keyPairs.Length / 2 >= MIN_MATCH_COUNTS)
            {
                return true;
            }
            else
            {
                if (bestKeypoints != keypoints && bestDescriptors != descriptors)
                {
                    keyPairs = FindPairs(this.bestKeypoints, this.bestDescriptors, newObj.keypoints, newObj.descriptors);
                    if (keyPairs.Length / 2 >= MIN_MATCH_COUNTS)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void CopyBestFeature(SurfTrackingObject oldObj)
        {
            if (oldObj.bestKeypoints != null && oldObj.bestDescriptors != null)
            {
                if (this.bestKeypoints.Length < oldObj.bestKeypoints.Length)
                {
                    this.bestKeypoints = oldObj.bestKeypoints;
                    this.bestDescriptors = oldObj.bestDescriptors;
                }
            }
            else throw new Exception("Best Feature has to be set");
        }


        private static int[] FindPairs(CvSURFPoint[] srcKeypoints, float[][] srcDescriptors, CvSURFPoint[] dstKeypoints, float[][] dstDescriptors)
        {
            List<int> ptPairs = new List<int>();

            for (int i = 0; i < srcDescriptors.Length; i++)
            {
                CvSURFPoint kp = srcKeypoints[i];
                int nearestNeighbor = NaiveNearestNeighbor(srcDescriptors[i], kp.Laplacian, dstKeypoints, dstDescriptors);
                if (nearestNeighbor >= 0)
                {
                    ptPairs.Add(i);
                    ptPairs.Add(nearestNeighbor);
                }
            }
            return ptPairs.ToArray();
        }

        private static int NaiveNearestNeighbor(float[] vec, int laplacian, CvSURFPoint[] modelKeypoints, float[][] modelDescriptors)
        {
            int neighbor = -1;
            double dist1 = 1e6, dist2 = 1e6;

            for (int i = 0; i < modelDescriptors.Length; i++)
            {
                // const CvSURFPoint* kp = (const CvSURFPoint*)kreader.ptr;
                CvSURFPoint kp = modelKeypoints[i];

                if (laplacian != kp.Laplacian)
                    continue;

                double d = CompareSurfDescriptors(vec, modelDescriptors[i], dist2);
                if (d < dist1)
                {
                    dist2 = dist1;
                    dist1 = d;
                    neighbor = i;
                }
                else if (d < dist2)
                    dist2 = d;
            }
            if (dist1 < 0.7 * dist2)
                return neighbor;
            else
                return -1;
        }

        private static double CompareSurfDescriptors(float[] d1, float[] d2, double best)
        {
            double totalCost = 0;

            for (int i = 0; i < d1.Length; i += 4)
            {
                double t0 = d1[i] - d2[i];
                double t1 = d1[i + 1] - d2[i + 1];
                double t2 = d1[i + 2] - d2[i + 2];
                double t3 = d1[i + 3] - d2[i + 3];
                totalCost += t0 * t0 + t1 * t1 + t2 * t2 + t3 * t3;
                if (totalCost > best)
                    break;
            }
            return totalCost;
        }


    }
}
