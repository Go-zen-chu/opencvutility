﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OpenCvUtility.Image;

namespace OpenCvUtility.Track
{
    // Class for calculating convex full of Points
    public class ConvexHull
    {
        Rect areaRect;
        public Rect AreaRect { get { return areaRect; } }

        double area = -1;
        public double Area
        {
            get
            {
                if (area >= 0) return area;
                var basePoint = chPoints[0];
                area = 0;
                for (int i = 1; i < chPoints.Count - 1; i++)
                {
                    // calc each triangles area and summaraize
                    area += ImageUtil.CrossProductPoints(basePoint, chPoints[i], chPoints[i + 1]) / 2;
                }
                return area;
            }
        }

        double circumference = -1;
        public double Circumference
        {
            get
            {
                if (circumference >= 0) return circumference;
                circumference = Math.Sqrt(ImageUtil.SquaredDistance(chPoints.Last(), chPoints[0]));
                for (int i = 0; i < chPoints.Count - 1; i++)
                {
                    circumference += Math.Sqrt(ImageUtil.SquaredDistance(chPoints[i], chPoints[i + 1]));
                }
                return circumference;
            }
        }

        public double Roundness
        {
            get { return 4 * Math.PI * Area / (Circumference * Circumference); }
        }

        List<Point> chPoints;
        public List<Point> CHPoints { get { return chPoints; } }

        private ConvexHull() { }

        public static ConvexHull GetConvexHull(IEnumerable<Point> sourcePoints)
        {
            if (sourcePoints.Count() <= 2) throw new Exception("Convexhull cannot calculate from less than 3 point data.");
            var ch = new ConvexHull();
            // select polar
            sourcePoints = sourcePoints.OrderBy(p => p.Y).ThenBy(p => p.X);
            var polar = sourcePoints.First();
            var xAxisVector = new Vector(1, 0); // unit vector of x axis
            // calclate angle of point-polar and x axis and sort them
            var angleOrderedPoints = sourcePoints.Skip(1).OrderByDescending(p => Vector.AngleBetween((p - polar), xAxisVector));
            ch.chPoints = GrahamScan(polar, angleOrderedPoints);

            // get min and max of points on convex hull
            var xMin = double.MaxValue;
            var xMax = double.MinValue;
            var yMin = polar.Y;
            var yMax = double.MinValue;
            foreach (var point in ch.chPoints)
            {
                if (point.X < xMin) xMin = point.X;
                if (point.X > xMax) xMax = point.X;
                if (point.Y > yMax) yMax = point.Y;
            }
            ch.areaRect = new Rect(new Point(xMin, yMin), new Point(xMax, yMax));
            return ch;
        }

        static List<Point> GrahamScan(Point polar, IEnumerable<Point> angleOrderedPoints)
        {
            var convexHullPoints = new List<Point>();
            var pointsArr = angleOrderedPoints.ToArray();
            convexHullPoints.Add(polar); // add polar to convex hull
            convexHullPoints.Add(pointsArr[0]); // add first point
            for (var pIdx = 1; pIdx < pointsArr.Length; pIdx++)
            {
                // compute whether the vector p1p2 and p1p3 is bending lefthand of righthand
                while (ImageUtil.CrossProductPoints(convexHullPoints[convexHullPoints.Count - 2], convexHullPoints[convexHullPoints.Count - 1], pointsArr[pIdx]) < 0)
                {
                    if (convexHullPoints.Count > 1)
                        convexHullPoints.RemoveAt(convexHullPoints.Count - 1); // remove last convexHullPoint
                    else if (pIdx == pointsArr.Length - 1)
                        break; // all points are colinear
                    else
                        pIdx++;
                }
                convexHullPoints.Add(pointsArr[pIdx]);
            }
            return convexHullPoints;
        }

        public bool IsInConvexHull(Point p)
        {
            if (areaRect.Contains(p) == false) return false;
            var isInPolygon = false;
            // concern the triangle p, chPoint[i], chPoint[i+1].  
            int j = chPoints.Count - 1;
            for (int i = 0; i < chPoints.Count; i++)
            {
                if (chPoints[i].Y > p.Y == chPoints[j].Y > p.Y) continue;
                if ((p.X - chPoints[i].X) * (chPoints[j].Y - chPoints[i].Y) < (p.Y - chPoints[i].Y) * (chPoints[j].X - chPoints[i].X))
                    isInPolygon = !isInPolygon;
                j = i;
            }
            return isInPolygon;
        }

        public Canvas GetConvexHullCanvas(int width, int height)
        {
            var canvas = new Canvas() { Width = width, Height = height };
            var polygon = new Polygon() { Points = new PointCollection(chPoints), StrokeThickness = 5, Stroke = Brushes.LightGreen };
            canvas.Children.Add(polygon);
            double thickness = 3;
            foreach (var pt in chPoints)
            {
                var ellipse = new Ellipse() { Stroke = Brushes.Red, StrokeThickness = thickness };
                Canvas.SetLeft(ellipse, pt.X - thickness /2);
                Canvas.SetTop(ellipse, pt.Y - thickness /2);
                canvas.Children.Add(ellipse);
            }
            return canvas;
        }
    }
}
