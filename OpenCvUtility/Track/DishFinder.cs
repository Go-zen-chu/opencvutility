﻿using OpenCvSharp;
using OpenCvUtility.Depth;
using OpenCvUtility.Image;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace OpenCvUtility.Track
{
    public static class DishFinder
    {
        public enum DishType { Circle, Polygon }

        public class Dish : SurfTrackingObject
        {
            public override double MAX_MOVING_SQUARE_SPEED
            {
                get
                {
                    return 2500;
                }
            }
            public override double MAX_SIZE_DIFF_RATE
            {
                get
                {
                    return 1.3;
                }
            }
            public override int UNDETECTED_LIMIT
            {
                get
                {
                    return 3;
                }
            }
            public override int WELL_MATCH_COUNT
            {
                get
                {
                    return 90; // 90回検知されば信頼できる
                }
            }

            public static readonly int MinRadius = 30;
            public static readonly int MaxRadius = 150;

            public DishType Type { get; set; }
            public double Radius { get; set; }
            public Point[] Edges { get; set; }


            public void DrawDish(IplImage bgrSrc)
            {
                if (bgrSrc.NChannels != 3) throw new Exception("Number of channels have to be 3");
                switch (this.Type)
                {
                    case DishType.Circle:
                        bgrSrc.DrawCircle((int)X, (int)Y, (int)Radius, CvColor.Red, 3);
                        return;
                    case DishType.Polygon:
                        bgrSrc.DrawLine((int)Edges.Last().X, (int)Edges.Last().Y, (int)Edges[0].X, (int)Edges[0].Y, CvColor.Blue, 3);
                        for (int edgeIdx = 1; edgeIdx < Edges.Length; edgeIdx++)
                        {
                            bgrSrc.DrawLine((int)Edges[edgeIdx - 1].X, (int)Edges[edgeIdx - 1].Y, (int)Edges[edgeIdx].X, (int)Edges[edgeIdx].Y, CvColor.Blue, 3);
                        }
                        return;
                }
            }
        }

        public class DishTracker
        {
            public bool IsDetected { get; set; }
            public bool IsTrackingReady { get; set; }
            public Rect ROI { get; private set; }
            public List<Dish> Dishes { get; private set; }

            int imgWidth;
            int imgHeight;

            public DishTracker(int imgWidth, int imgHeight, Rect roi)
            {
                IsDetected = false;
                IsTrackingReady = false;
                this.imgWidth = imgWidth;
                this.imgHeight = imgHeight;
                this.ROI = roi;
            }

            public void Detect(short[] depthData, PlaneFinder.Plane detectedPlane, short fromDeskMin = 15, short fromDeskMax = 200)
            {
                IsDetected = true;
                Task.Factory.StartNew(() =>
                {
                    Dishes = DetectDishLikeObjects(depthData, imgWidth, imgHeight, detectedPlane, ROI, fromDeskMin, fromDeskMax);
                    IsTrackingReady = true;
                });
            }

            public void Track(short[] depthData, PlaneFinder.Plane detectedPlane, short fromDeskMin = 15, short fromDeskMax = 200)
            {
                IsTrackingReady = false;
                Task.Factory.StartNew(() =>
                {
                    Dishes = TrackDishes(depthData, imgWidth, imgHeight, detectedPlane, ROI, Dishes, fromDeskMin, fromDeskMax);
                    Console.WriteLine("Num Dishes " + Dishes.Count);
                    IsTrackingReady = true;
                });
            }

            public Canvas DrawDishes(Canvas canvas)
            {
                if (IsTrackingReady)
                {
                    canvas.Children.Clear();
                    foreach (var dish in Dishes)
                    {
                        canvas = dish.DrawBoundingRect(canvas);
                    }
                }
                return canvas;
            }
        }

        // 皿をトラッキングする
        public static List<Dish> TrackDishes(short[] depthData, int width, int height, PlaneFinder.Plane detectedPlane, Rect roi, List<Dish> trackingDishes, short fromDeskMin = 15, short fromDeskMax = 200)
        {
            // 頻繁に検出されない皿を削除
            var notFreqDetectedDishes = new List<Dish>();
            foreach (var oldDish in notFreqDetectedDishes)
            {
                if (oldDish.IsLonglyUndetected() && oldDish.IsWellMatched() == false) notFreqDetectedDishes.Add(oldDish);
            }
            notFreqDetectedDishes.ForEach(d => trackingDishes.Remove(d));

            var detectedDishes = DetectDishLikeObjects(depthData, width, height, detectedPlane, roi, fromDeskMin, fromDeskMax);
            // 過去の皿とのマッチング
            foreach (var oldDish in trackingDishes)
            {
                Dish matchedNewDish = null;
                foreach (var newDish in detectedDishes)
                {
                    if (oldDish.IsSameObject(newDish))
                    {
                        matchedNewDish = newDish;
                        Console.WriteLine("Matched dish " + oldDish.ID);
                        break;
                    }
                }
                if (matchedNewDish != null)
                { // マッチした皿が見つかった
                    matchedNewDish.ID = oldDish.ID;
                    matchedNewDish.CopyBestFeature(oldDish); // 最もよい状態を保持するか決める
                    matchedNewDish.AddDetectedTimes(oldDish); // 過去の発見時刻を移す
                    detectedDishes.Remove(matchedNewDish);
                }
                else
                { //マッチしなかった
                    oldDish.AddUndetectedTimes();
                }
            }
            trackingDishes.AddRange(detectedDishes); // 新しく検出された皿を追加する
            return trackingDishes;
        }

        // 深さデータから皿らしいものを検出。深さ情報には数cmのノイズがあるため、精度は低い。
        public static List<Dish> DetectDishLikeObjects(short[] depthData, int width, int height, PlaneFinder.Plane detectedPlane, Rect roi, short fromDeskMin = 15, short fromDeskMax = 200)
        {
            var depthIpl = CreateDeskTopImage(depthData, width, height, detectedPlane, roi, fromDeskMin, fromDeskMax);
            //General.ShowImage(depthIpl);
            depthIpl.EqualizeHist(depthIpl);
            //General.ShowImage(depthIpl);

            var depthCannyImg = new IplImage(depthIpl.Size, depthIpl.Depth, 1);
            depthCannyImg.Zero();
            depthIpl.Canny(depthCannyImg, 20, 40); // かなりゆるいエッジ検出で平皿も検出対象にする
            //General.ShowImage(depthCannyImg);
            var dishLikeObjectsRect = Shapes.FindContoursBoundingRect(depthCannyImg, 600, 10000);
            //foreach (var r in dishLikeObjectsRect)
            //    Cv.DrawRect(depthCannyImg, r, new CvScalar(255));
            //General.ShowImage(depthCannyImg);
            depthCannyImg.Dispose();

            var detectedDishLikeObjs = new List<Dish>();
            foreach (var br in dishLikeObjectsRect)
            {
                var dish = new Dish() { X = (br.Right - br.Left) / 2.0, Y = (br.Bottom - br.Top) / 2.0, BoundingRect = new Rect(br.Left, br.Top, br.Width, br.Height) };
                using (var dishImg = ImageUtil.CropImage(depthIpl, br))
                {
                    Cv.EqualizeHist(dishImg, dishImg);
                    dish.CalcSurf(dishImg);
                    // Keypoint の数が小さいと検出しない
                    if (dish.NumKeypoints >= Dish.MIN_MATCH_COUNTS)
                        detectedDishLikeObjs.Add(dish);
                }
            }
            if (depthIpl != null) depthIpl.Dispose();
            return detectedDishLikeObjs;
        }

        // 深さデータから皿を検出する際に机の色を消す。プロジェクタ描画の場合、机と皿の色が区別できず使えない
        public static List<Dish> DetectDishesRemoveDesk(IplImage deskImg, short[] depthData, PlaneFinder.Plane detectedPlane, Rect roi, Color deskColor, short fromDeskMin = 15, short fromDeskMax = 200)
        {
            DepthUtil.ExtractByPlaneOffset(deskImg, deskImg, depthData, detectedPlane, roi, fromDeskMin, fromDeskMax);
            ImageUtil.ShowImage(deskImg);
            var deskHsv = Hsv.FromRgb(deskColor);
            var deskColorRemovedImg = ExtractColor.ExtractSingleColor(deskImg, new double[] { deskHsv.H - 10, deskHsv.S - 0.1, deskHsv.V - 0.4 }, new double[] { deskHsv.H + 10, deskHsv.S + 0.1, deskHsv.V + 0.1 }, true);
            ImageUtil.ShowImage(deskColorRemovedImg);

            var bgrSplitImgs = deskImg.Split(); // split bgr
            var bgrCannyImg = new IplImage(deskImg.Size, deskImg.Depth, 1);
            foreach (var img in bgrSplitImgs)
            {
                using (var tmp = new IplImage(deskImg.Size, BitDepth.U8, 1))
                {
                    tmp.Zero();
                    img.Canny(tmp, 100, 200);
                    Cv.Or(bgrCannyImg, tmp, bgrCannyImg);
                }
            }
            ImageUtil.ShowImage(bgrCannyImg);
            Filter.Morphology(bgrCannyImg, bgrCannyImg, 5, MorphologyOperation.Close);
            ImageUtil.ShowImage(bgrCannyImg);


            var detectedDishes = new List<Dish>();
            // 円検出
            var detectCircles = Shapes.DetectCircles(bgrCannyImg, Dish.MinRadius, Dish.MaxRadius);
            //foreach (var circleNullable in detectCircles)
            //{
            //    if (circleNullable != null)
            //    {
            //        var circle = circleNullable.Value;
            //        var dish = new Dish() { X = circle.Center.X, Y = circle.Center.Y, Radius = circle.Radius, Type = DishType.Circle };
            //        var dishROI = new CvRect((int)(circle.Center.X - circle.Radius), (int)(circle.Center.Y - circle.Radius), (int)(circle.Radius * 2), (int)(circle.Radius * 2));
            //        var dishImg = General.CropImage(deskImg, dishROI);
            //        dish.CalcSurf(dishImg);
            //        detectedDishes.Add(dish);

            //        using (IplImage tmp = new IplImage(deskImg.Size, BitDepth.U8, 3))
            //        {
            //            tmp.Zero();
            //            tmp.DrawCircle(circle.Center, (int)circle.Radius, CvColor.Red);
            //            General.ShowImage(tmp);
            //        }
            //    }
            //}
            // 矩形検出
            var detectPoly = Shapes.DetectPolygon(bgrCannyImg, 4);
            foreach (var poly in detectPoly)
            {
                var dish = new Dish() { X = poly.Average(p => p.X), Y = poly.Average(p => p.Y), Edges = poly.Select(p => new Point(p.X, p.Y)).ToArray(), Type = DishType.Polygon };
                int xMin = int.MaxValue; int yMin = int.MaxValue; int xMax = int.MinValue; int yMax = int.MinValue;
                foreach (var p in poly)
                {
                    if (p.X < xMin) xMin = (int)p.X;
                    if (p.Y < yMin) yMin = (int)p.Y;
                    if (p.X > xMax) xMax = (int)p.X;
                    if (p.Y > yMax) yMax = (int)p.Y;
                }
                var dishROI = new CvRect(xMin, yMin, xMax - xMin, yMax - yMin);
                var dishImg = ImageUtil.CropImage(deskImg, dishROI);
                dish.CalcSurf(dishImg);
                detectedDishes.Add(dish);
            }
            if (deskImg != null) deskImg.Dispose();
            return detectedDishes;
        }

        public static IplImage CreateDeskTopImage(short[] depthData, int width, int height, PlaneFinder.Plane detectedPlane, Rect roi, short fromDeskMin, short fromDeskMax)
        {
            var depthIpl = Cv.CreateImage(new CvSize(width, height), BitDepth.U8, 1);
            depthIpl.Zero();
            var depthIplIntPtr = depthIpl.ImageData;
            Parallel.For(0, height, row =>
            {
                var depthIdx = row * width;
                for (int col = 0; col < width; col++)
                {
                    var point = new Point(col, row);
                    var planeZ = detectedPlane.CalcZValue(point);
                    var depth = depthData[depthIdx];
                    if ((planeZ - fromDeskMax) < depth && depth < planeZ - fromDeskMin && roi.Contains(point))
                    { // inside plane and within 100 mm
                        Marshal.WriteByte(depthIplIntPtr, depthIdx, (byte)(planeZ - depth));
                    }
                    depthIdx++;
                }
            });
            return depthIpl;
        }

        // 机上深さ画像の深さデータを閾値で正規化する
        public static IplImage NormalizeDeskTopImage(IplImage deskTopImg, byte fromDeskMax)
        {
            if (deskTopImg.NChannels != 1) throw new Exception("deskTopImg has to be 1 channel");
            var normalizedIpl = Cv.CreateImage(deskTopImg.Size, BitDepth.U8, 1);
            normalizedIpl.Zero();
            var deskTopIntPtr = deskTopImg.ImageData;
            var normalizedIntPtr = normalizedIpl.ImageData;
            var height = deskTopImg.Size.Height;
            var width = deskTopImg.Size.Width;
            Parallel.For(0, height, row =>
            {
                var depthIdx = row * width;
                for (int col = 0; col < width; col++)
                {
                    var fromDeskVal = Marshal.ReadByte(deskTopIntPtr, depthIdx);
                    if (fromDeskVal != 0)
                    {
                        if (fromDeskVal <= fromDeskMax) // 正規化する領域内
                        {
                            byte newByte = (byte)(255 * fromDeskVal / (float)fromDeskMax);
                            Marshal.WriteByte(normalizedIntPtr, depthIdx, newByte);
                        }
                        else
                        {
                            Marshal.WriteByte(normalizedIntPtr, depthIdx, 0);
                        }
                    }
                    depthIdx++;
                }
            });
            return normalizedIpl;
        }


    }
}
