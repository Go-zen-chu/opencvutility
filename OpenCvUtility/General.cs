﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Script.Serialization;

namespace OpenCvUtility
{
    public static class General
    {

        public static void ExportDataBinary<T>(string exportPath, T obj)
        {
            using (var fs = new FileStream(exportPath, FileMode.Create, FileAccess.Write))
            {
                var bf = new BinaryFormatter();
                bf.Serialize(fs, obj);
            }
        }
        public static T ImportBinaryData<T>(string importPath)
        {
            using (var fs = new FileStream(importPath, FileMode.Open, FileAccess.Read))
            {
                var bf = new BinaryFormatter();
                return (T)bf.Deserialize(fs);
            }
        }

        /// <summary>
        /// Export obj in Json format. However, types like Dictionary cannot be exported. Use ExportDataContractJson.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exportPath"></param>
        /// <param name="obj"></param>
        public static void ExportDataJson<T>(string exportPath, T obj)
        {
            var serializer = new JavaScriptSerializer();
            using (var sw = new StreamWriter(exportPath))
                sw.Write(serializer.Serialize(obj));
        }
        /// <summary>
        /// Import exported Json data and cast it to T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="importPath"></param>
        /// <returns></returns>
        public static T ImportJsonData<T>(string importPath)
        {
            var serializer = new JavaScriptSerializer();
            using (var sr = new StreamReader(importPath))
                return serializer.Deserialize<T>(sr.ReadToEnd());
        }

        public static void ExportDataContractJson<ContractT>(string exportPath, ContractT obj)
        {
            var serializer = new DataContractJsonSerializer(typeof(ContractT));
            using (var fs = new FileStream(exportPath, FileMode.Create, FileAccess.Write))
            {
                serializer.WriteObject(fs, obj);
            }
        }
        public static ContractT ImportContractJsonData<ContractT>(string importPath)
        {
            var serializer = new DataContractJsonSerializer(typeof(ContractT));
            using (var fs = new FileStream(importPath, FileMode.Open, FileAccess.Read))
            {
                return (ContractT)serializer.ReadObject(fs);
            }
        }
    }
}
