﻿using OpenCvSharp;
using OpenCvSharp.CPlusPlus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCvUtility.ML
{
    public class RandomForest : Classifier
    {
        public static CvRTParams DefaultRTreesParams = new CvRTParams(
            20, //max depth
            10, // min sample count(1% of total instances is normal)
            0, // regression accuracy: N/A here
            true, // compute surrogate split, if we have missing data
            6, // max category num(used for clustering)
            null,  // the array of priors, the bigger weight, the more attention
            true, // calculate variable importance
            0,
            new CvTermCriteria(CriteriaType.Iteration, 10, 0.05)
        );

        CvRTrees rTrees;

        private RandomForest() { }

        public static RandomForest Build(MLUtil.FeatureData trainingData, string modelSavingPath = null, CvRTParams rtParams = null)
        {
            if (trainingData.Features == null || trainingData.Classes == null) throw new ArgumentNullException();
            if (trainingData.Features.Rows != trainingData.Classes.Rows) throw new ArgumentException("length of features and answers should be equal");
            int featureDimension = trainingData.Features.Cols;
            int totalTrainingDataNum = trainingData.Features.Rows;
            // varType defines what kind of data(e.g. numerical, categorical) the feature is.
            using (var varType = new CvMat(featureDimension + 1, 1, MatrixType.U8C1))
            using (var sampleIdx = new CvMat(1, totalTrainingDataNum, MatrixType.U8C1))
            {
                // CV_VAR_ORDERED is same as CV_VAR_NUMERICAL
                varType.Set(CvScalar.ScalarAll(CvStatModel.CV_VAR_ORDERED));
                // Catrgorical value for last column
                varType.SetReal1D(featureDimension, CvStatModel.CV_VAR_CATEGORICAL);
                CvMat mat;
                Cv.GetCols(sampleIdx, out mat, 0, totalTrainingDataNum);
                // Decide which sample we use. We set all 1 for training data.
                mat.Set(CvScalar.RealScalar(1));
                if (rtParams == null)
                {
                    rtParams = DefaultRTreesParams;
                    rtParams.MinSampleCount = totalTrainingDataNum / 100; //(1% of total instances is normal)
                    rtParams.MaxCategories = trainingData.ClassIdxDict.Count / 2;
                }
                if (trainingData.HasMissingValues() == false) rtParams.UseSurrogates = false;
                var rf = new RandomForest();
                var rTrees = new CvRTrees();
                rTrees.Train(trainingData.Features, DTreeDataLayout.RowSample, trainingData.Classes, null, sampleIdx, varType, trainingData.MissingValues, rtParams);
                if (modelSavingPath != null) rTrees.Save(modelSavingPath);
                mat.Dispose();
                rf.rTrees = rTrees;
                return rf;
            }
        }

        public static RandomForest Load(string modelFilePath)
        {
            var rf = new RandomForest();
            var rTrees = new CvRTrees();
            rTrees.Load(modelFilePath);
            if (rTrees.GetTreeCount() == 0) throw new Exception("Build random forest failed");
            rf.rTrees = rTrees;
            return rf;
        }

        public override double Classify(CvMat classifyingFeature)
        {
            return rTrees.Predict(classifyingFeature);
        }
        public override double Classify(CvMat classifyingFeatures, CvMat missingMask)
        {
            return rTrees.Predict(classifyingFeatures, missingMask);
        }

        public override void Dispose()
        {
            if (rTrees != null) rTrees.Dispose();
        }
    }
}
