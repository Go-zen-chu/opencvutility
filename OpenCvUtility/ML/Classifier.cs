﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCvUtility.ML
{
    public abstract class Classifier : IDisposable
    {
        public abstract double Classify(CvMat classifyingFeature);
        public abstract double Classify(CvMat classifyingFeature, CvMat missingMask);
        public virtual int[,] GetConfusionMatrix(OpenCvUtility.ML.MLUtil.FeatureData testData)
        {
            if (testData == null) throw new ArgumentNullException();
            if (testData.ClassIdxDict == null) throw new Exception("Class index dictionary has to be set in classification");
            
            var confusionMatrix = new int[testData.ClassIdxDict.Count, testData.ClassIdxDict.Count];
            int featuresNum = testData.Features.Rows;
            int featureDim = testData.Features.Cols;
            // Create 1d float mat for classification(CvMat.GetRow creates 2d mat)
            using (var featureDatum = new CvMat(1, featureDim, MatrixType.F32C1))
            using (var missingDatum = new CvMat(1, featureDim, MatrixType.U8C1))
            {
                for (int rowIdx = 0; rowIdx < featuresNum; rowIdx++)
                {
                    int predictionIdx = int.MaxValue;
                    for (int featureIdx = 0; featureIdx < featureDim; featureIdx++)
                    {
                        unsafe
                        {
                            float* f = featureDatum.DataSingle + featureIdx;
                            *f = (float)testData.Features[rowIdx, featureIdx];
                            if (testData.HasMissingValues())
                            {
                                byte* m = missingDatum.DataByte + featureIdx;
                                *m = (byte)testData.MissingValues[rowIdx, featureIdx];
                            }
                        }
                    }
                    predictionIdx = (int)(testData.HasMissingValues() ? this.Classify(featureDatum, missingDatum) : this.Classify(featureDatum));
                    var classIdx = (int)testData.Classes[rowIdx];
                    confusionMatrix[classIdx, predictionIdx] += 1;
                }
            }
            return confusionMatrix;
        }
        public virtual string GetEvaluationString(OpenCvUtility.ML.MLUtil.FeatureData testData)
        {
            int featuresNum = testData.Features.Rows;
            var confusionMatrix = GetConfusionMatrix(testData);
            var correctNum = 0;
            for (int row = 0; row < confusionMatrix.GetLength(0); row++)
            {
                correctNum += confusionMatrix[row, row]; // correct num is in diagnal line
            }
            var confusionMatrixBuilder = new StringBuilder();
            for (int row = 0; row < confusionMatrix.GetLength(0); row++)
            {
                confusionMatrixBuilder.Append(confusionMatrix[row, 0]);
                for (int col = 1; col < confusionMatrix.GetLength(1); col++)
                    confusionMatrixBuilder = confusionMatrixBuilder.Append(",").Append(confusionMatrix[row, col]);
                confusionMatrixBuilder = confusionMatrixBuilder.Append(Environment.NewLine);
            }
            return string.Join(Environment.NewLine, "Total tested feature:" + featuresNum,
                                                    "Correct predictions:" + correctNum,
                                                    "Accuracy:" + ((double)correctNum / featuresNum),
                                                    "Confusion Matrix:",
                                                    confusionMatrixBuilder.ToString());
        }
        public virtual void ExportEvaluationString(OpenCvUtility.ML.MLUtil.FeatureData testData, string resultExportFilePath)
        {
            var evaluationStr = GetEvaluationString(testData);
            using (var sw = new StreamWriter(resultExportFilePath)) sw.WriteLine(evaluationStr);
        }
        public abstract void Dispose();
    }
}
