﻿using OpenCvSharp;
using OpenCvSharp.CPlusPlus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCvUtility.ML
{
    public class CustomRandomForest : Classifier
    {
        CvDTree[] dTrees;
        int[][] rndAttrIdxs;

        public int NumTrees { get; private set; }
        public int NumAttributes { get; private set; }
        public int NumClass { get; private set; }

        private CustomRandomForest(int numTrees, int numRndAttrs, int numClass)
        {
            this.NumTrees = numTrees;
            this.NumAttributes = numRndAttrs;
            this.NumClass = numClass;
            dTrees = new CvDTree[numTrees];
            rndAttrIdxs = new int[numTrees][];
        }

        public static CustomRandomForest Build(MLUtil.FeatureData trainingData, int numTrees, int numRndAttrs, string modelSavingPath, CvDTreeParams dtreeParams = null)
        {
            if (trainingData.Features == null || trainingData.Classes == null) throw new ArgumentNullException();
            if (trainingData.Features.Rows != trainingData.Classes.Rows) throw new ArgumentException("length of features and answers should be equal");
            int featureDimension = trainingData.Features.Cols;
            int totalTrainingDataNum = trainingData.Features.Rows;
            if (featureDimension < numRndAttrs) throw new ArgumentException("numRndAttrs has to be smaller than training data's feature dimension");
            if (dtreeParams == null)
            {
                dtreeParams = DecisionTree.DefaultDTreeParams;
                dtreeParams.MinSampleCount = totalTrainingDataNum / 100;
                dtreeParams.MaxCategories = trainingData.ClassIdxDict.Count / 2;
            }
            if (trainingData.HasMissingValues() == false) dtreeParams.UseSurrogates = false;
            var crf = new CustomRandomForest(numTrees, numRndAttrs, trainingData.ClassIdxDict.Count);
            var dirPath = Path.GetDirectoryName(modelSavingPath);
            var baseName = Path.Combine(dirPath, Path.GetFileNameWithoutExtension(modelSavingPath));
            var modelExt = Path.GetExtension(modelSavingPath);
            //Parallel.For(0, numTrees, treeIdx =>
            for(int treeIdx = 0; treeIdx < numTrees; treeIdx++)
            {
                // Random sample Attributes for making random forest
                var rndSampleFeatureDataTpl = MLUtil.RandomSampleAttributes(trainingData, numRndAttrs);
                var rndFd = rndSampleFeatureDataTpl.Item1;
                var rndIdxs = rndSampleFeatureDataTpl.Item2;
                crf.rndAttrIdxs[treeIdx] = rndIdxs;
                var fileNameBase = new StringBuilder(baseName).Append("_").Append(treeIdx).ToString();
                General.ExportDataJson<int[]>(fileNameBase + ".json", rndSampleFeatureDataTpl.Item2);
                crf.dTrees[treeIdx] = DecisionTree.buildDTree(rndFd, fileNameBase + modelExt, dtreeParams);
            }//);
            var paramDict = new Dictionary<string, int>() { { "numClass", trainingData.ClassIdxDict.Count }, { "numTrees", numTrees }, { "numRndAttrs", numRndAttrs } };
            General.ExportDataJson<Dictionary<string, int>>(Path.Combine(dirPath, "params.json"), paramDict);
            return crf;
        }

        /// <summary>
        /// Build DTree using trainingData separately.
        /// </summary>
        /// <param name="trainingData"></param>
        /// <param name="numRndAttrs"></param>
        /// <param name="modelSavingPath"></param>
        /// <param name="dtreeParams"></param>
        public static void Build_Partial(MLUtil.FeatureData trainingData, int numRndAttrs, string modelSavingPath, CvDTreeParams dtreeParams = null)
        {
            if (trainingData.Features == null || trainingData.Classes == null) throw new ArgumentNullException();
            if (trainingData.Features.Rows != trainingData.Classes.Rows) throw new ArgumentException("length of features and answers should be equal");
            int featureDimension = trainingData.Features.Cols;
            int totalTrainingDataNum = trainingData.Features.Rows;
            if (featureDimension < numRndAttrs) throw new ArgumentException("numRndAttrs has to be smaller than training data's feature dimension");
            
            if (dtreeParams == null)
            {
                dtreeParams = DecisionTree.DefaultDTreeParams;
                dtreeParams.MinSampleCount = totalTrainingDataNum / 100;
                dtreeParams.MaxCategories = trainingData.ClassIdxDict.Count / 2;
            }
            if (trainingData.HasMissingValues() == false) dtreeParams.UseSurrogates = false;

            var dirPath = Path.GetDirectoryName(modelSavingPath);
            var baseName = Path.Combine(dirPath, Path.GetFileNameWithoutExtension(modelSavingPath));
            var modelExt = Path.GetExtension(modelSavingPath);

            var builtModelPaths = Directory.GetFiles(dirPath, "*" + modelExt, SearchOption.TopDirectoryOnly);

            // Random sample Attributes for making random forest
            var rndSampleFeatureDataTpl = MLUtil.RandomSampleAttributes(trainingData, numRndAttrs);
            var rndFd = rndSampleFeatureDataTpl.Item1;
            var rndIdxs = rndSampleFeatureDataTpl.Item2;
            var treeIdx = builtModelPaths.Length; // set tree idx as the number of already built trees
            var fileNameBase = new StringBuilder(baseName).Append("_").Append(treeIdx).ToString();
            General.ExportDataJson<int[]>(fileNameBase + ".json", rndSampleFeatureDataTpl.Item2);
            var dtree = DecisionTree.buildDTree(rndFd, fileNameBase + modelExt, dtreeParams);
            dtreeStore.Add(dtree);

            var paramDictJsonPath = Path.Combine(dirPath, "params.json");
            Dictionary<string, int> paramDict = null;
            if(File.Exists(paramDictJsonPath)){
                paramDict = General.ImportJsonData<Dictionary<string, int>>(paramDictJsonPath);
                paramDict["numTrees"] += 1; // increment number of trees
            }else{
                paramDict = new Dictionary<string, int>() { { "numClass", trainingData.ClassIdxDict.Count }, { "numTrees", 1 }, { "numRndAttrs", numRndAttrs } };
            } 
            General.ExportDataJson<Dictionary<string, int>>(paramDictJsonPath, paramDict);
        }
        // Notourious variable that stores DTree in order not to raise EntryNotFoundException when disposing CvDTree which is a bug of library
        static List<CvDTree> dtreeStore = new List<CvDTree>();


        public static CustomRandomForest Load(string modelFilePath)
        {
            var dirPath = Path.GetDirectoryName(modelFilePath);
            var paramDict = General.ImportJsonData<Dictionary<string, int>>(Path.Combine(dirPath, "params.json"));
            var numTrees = paramDict["numTrees"];
            var numRndAttrs = paramDict["numRndAttrs"];
            var numClass = paramDict["numClass"];
            var crf = new CustomRandomForest(numTrees, numRndAttrs, numClass);
            var baseName = Path.Combine(dirPath, Path.GetFileNameWithoutExtension(modelFilePath));
            var ext = Path.GetExtension(modelFilePath);
            for (int treeIdx = 0; treeIdx < numTrees; treeIdx++)
            {
                var fileNameBase = new StringBuilder(baseName).Append("_").Append(treeIdx);
                crf.rndAttrIdxs[treeIdx] = General.ImportJsonData<int[]>(fileNameBase + ".json");
                var dTree = new CvDTree();
                dTree.Load(fileNameBase + ext);
                crf.dTrees[treeIdx] = dTree;
            }
            return crf;
        }

        public Tuple<int, double> ClassProbability(CvMat classifyingFeature, CvMat missingMask)
        {
            var predictionVotes = VotePredictions(classifyingFeature, missingMask);
            int maxClsIdx = 0;
            int maxPredictionCount = predictionVotes[0];
            for (int clsIdx = 1; clsIdx < predictionVotes.Length; clsIdx++)
            {
                if (predictionVotes[clsIdx] > maxPredictionCount)
                {
                    maxPredictionCount = predictionVotes[clsIdx];
                    maxClsIdx = clsIdx;
                }
                predictionVotes[clsIdx] = 0; // initialize
            }
            return new Tuple<int,double>(maxClsIdx, ((double)maxPredictionCount) / NumTrees);
        }

        public override double Classify(CvMat classifyingFeature)
        {
            throw new NotImplementedException();
        }

        public override double Classify(CvMat classifyingFeature, CvMat missingMask)
        {
            return ClassProbability(classifyingFeature, missingMask).Item1;
        }

        public int[] VotePredictions(CvMat classifyingFeature, CvMat missingMask)
        {
            var predictionVotes = new int[NumClass];
            for (int treeIdx = 0; treeIdx < NumTrees; treeIdx++)
            {
                var rndAttrIdx = rndAttrIdxs[treeIdx];
                // extract feature according to random sampled attribute
                using (var rndFeature = new CvMat(1, NumAttributes, classifyingFeature.ElemType))
                using (var rndMissing = new CvMat(1, NumAttributes, missingMask.ElemType))
                {
                    unsafe
                    {
                        float* f = classifyingFeature.DataSingle;
                        float* rndF = rndFeature.DataSingle;
                        byte* m = missingMask.DataByte;
                        byte* rndM = rndMissing.DataByte;
                        for (int i = 0; i < NumAttributes; i++)
                        {
                            rndF[i] = f[rndAttrIdx[i]];
                            rndM[i] = m[rndAttrIdx[i]];
                        }
                    }
                    var dtree = dTrees[treeIdx];
                    var clsIdx = (int)dtree.Predict(rndFeature, rndMissing).Value;
                    predictionVotes[clsIdx]++; // increment class that predicted
                }
            }
            return predictionVotes;
        }

        public override void Dispose()
        {
            if (dTrees != null) 
                foreach(var dTree in dTrees)
                    dTree.Dispose();
        }
    }
}
