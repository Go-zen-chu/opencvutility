﻿using OpenCvSharp;
using OpenCvSharp.CPlusPlus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCvUtility.ML
{
    public class DecisionTree : Classifier
    {
        public static CvDTreeParams DefaultDTreeParams = new CvDTreeParams(
            20, // max depth
            10, // min sample count
            0, // regression accuracy: N/A here
            true, // compute surrogate split, if we have missing data
            6, // max number of categories (use sub-optimal algorithm for larger numbers)
            10, // the number of cross-validation folds
            false, // use 1SE rule => smaller tree
            true, // throw away the pruned tree branches
            null // the array of priors, the bigger weight, the more attention
        );

        CvDTree dTree;

        private DecisionTree() { }

        public static DecisionTree Build(MLUtil.FeatureData trainingData, string modelSavingPath = null, CvDTreeParams dtreeParams = null)
        {
            var dt = new DecisionTree();
            dt.dTree = buildDTree(trainingData, modelSavingPath, dtreeParams);
            return dt;
        }

        internal static CvDTree buildDTree(MLUtil.FeatureData trainingData, string modelSavingPath = null, CvDTreeParams dtreeParams = null)
        {
            if (trainingData.Features == null || trainingData.Classes == null) throw new ArgumentNullException();
            if (trainingData.Features.Rows != trainingData.Classes.Rows) throw new ArgumentException("length of features and answers should be equal");
            int featureDimension = trainingData.Features.Cols;
            int totalTrainingDataNum = trainingData.Features.Rows;
            using (var varType = new CvMat(featureDimension + 1, 1, MatrixType.U8C1)) // +1 is for class
            using (var sampleIdx = new CvMat(1, totalTrainingDataNum, MatrixType.U8C1))
            {
                // CV_VAR_ORDERED is same as CV_VAR_NUMERICAL
                varType.Set(CvScalar.ScalarAll(CvStatModel.CV_VAR_ORDERED));
                // Catrgorical value for last column which is class
                varType.SetReal1D(featureDimension, CvStatModel.CV_VAR_CATEGORICAL);
                CvMat mat;
                Cv.GetCols(sampleIdx, out mat, 0, totalTrainingDataNum);
                // Decide which sample we use. We set all 1 for training data.
                mat.Set(CvScalar.RealScalar(1));
                var dTree = new CvDTree();
                if (dtreeParams == null)
                {
                    dtreeParams = DefaultDTreeParams;
                    dtreeParams.MinSampleCount = totalTrainingDataNum / 100;
                    dtreeParams.MaxCategories = trainingData.ClassIdxDict.Count / 2;
                }
                if (trainingData.HasMissingValues() == false) dtreeParams.UseSurrogates = false;
                dTree.Train(trainingData.Features, DTreeDataLayout.RowSample, trainingData.Classes, null, sampleIdx, varType, trainingData.MissingValues, dtreeParams);
                if (modelSavingPath != null) dTree.Save(modelSavingPath);
                mat.Dispose();
                return dTree;
            }
        }

        public static DecisionTree Load(string modelFilePath)
        {
            var dt = new DecisionTree();
            var dTree = new CvDTree();
            dTree.Load(modelFilePath);
            dt.dTree = dTree;
            return dt;
        }

        public override double Classify(CvMat classifyingFeature)
        {
            return dTree.Predict(classifyingFeature).Value;
        }

        public override double Classify(CvMat classifyingFeature, CvMat missingMask)
        {
            return dTree.Predict(classifyingFeature, missingMask).Value;
        }

        public override void Dispose()
        {
            //WARNING: OpenCVSharp has a bug that EntryPointNotFoundException will cause when you dispose DTree
            if (dTree != null) dTree.Dispose();
        }
    }
}
