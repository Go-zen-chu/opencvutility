﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCvUtility.ML
{
    public static class MLUtil
    {
        public class FeatureData : IDisposable
        {
            public CvMat Features { get; set; } 
            public CvMat Classes { get; set; }
            public CvMat MissingValues { get; set; }
            /// <summary>
            /// Dictionary which makes each class related to index. Used in classification
            /// </summary>
            public Dictionary<string, int> ClassIdxDict { get; set; }

            public bool HasMissingValues()
            {
                return this.MissingValues != null;
            }

            public void Dispose()
            {
                if (this.Features != null) this.Features.Dispose();
                if (this.Classes != null) this.Classes.Dispose();
                if (this.MissingValues != null) this.MissingValues.Dispose();
            }
        }

        public static FeatureData LoadFeatureDataFile(string featureDataFilePath, int featuresNum, int featureDimension, Dictionary<string, int> classIdxDict)
        {
            // Extract these data from string
            var features = new CvMat(featuresNum, featureDimension, MatrixType.F32C1);
            var classes = new CvMat(featuresNum, 1, MatrixType.F32C1);
            var missingValues = new CvMat(featuresNum, featureDimension, MatrixType.U8C1);
            var featureData = new FeatureData() { Features = features, Classes = classes, MissingValues = missingValues, ClassIdxDict = classIdxDict };
            readFeatureDataFile(featureData, featureDataFilePath, featureDimension);
            return featureData;
        }

        // Load data parallely, filePathLineNumList is a dictionary which has file path and its line num
        public static FeatureData LoadFeatureDataFilesParallel(IEnumerable<Tuple<string, int>> filePathLineNumList, int featureDimension, Dictionary<string, int> classIdxDict)
        {
            var featuresNum = filePathLineNumList.Sum(tpl => tpl.Item2);
            // Extract these data from string
            var features = new CvMat(featuresNum, featureDimension, MatrixType.F32C1);
            var classes = new CvMat(featuresNum, 1, MatrixType.F32C1);
            var missingValues = new CvMat(featuresNum, featureDimension, MatrixType.U8C1);
            var featureData = new FeatureData() { Features = features, Classes = classes, MissingValues = missingValues, ClassIdxDict = classIdxDict };

            // calculate row idx of cvmat for processing parallely
            var lineNumSum = 0;
            var orderedList = filePathLineNumList.OrderBy(t => t.Item1)
                                .Select(t =>
                                {
                                    var tpl = new { path = t.Item1, lineNumSum };
                                    lineNumSum += t.Item2;
                                    return tpl;
                                });

            Parallel.ForEach(orderedList, tpl =>
            {
                readFeatureDataFile(featureData, tpl.path, featureDimension, tpl.lineNumSum);
            });
            return featureData;
        }

        static void readFeatureDataFile(FeatureData workingData, string featureDataFilePath, int featureDimension, int starRowIdx = 0)
        {
            string[] csvLine;
            // featureDimension * rowIdx will beyond 32bit size if you are working with large size data
            long rowIdx = starRowIdx;
            using (var sr = new StreamReader(featureDataFilePath))
            {
                unsafe
                {
                    while (sr.Peek() != -1)
                    {
                        csvLine = sr.ReadLine().Split(',');
                        if (csvLine.Length == 0) break;
                        float* clsRow = workingData.Classes.DataSingle + rowIdx;
                        *clsRow = (float)workingData.ClassIdxDict[csvLine[0]]; // set first col num as class data
                        // featureDimension * rowIdx will beyond int size if you are working with big data
                        float* featureRow = workingData.Features.DataSingle + (featureDimension * rowIdx);
                        byte* missingRow = workingData.MissingValues.DataByte + (featureDimension * rowIdx);
                        for (int i = 0; i < featureDimension; i++)
                        {
                            if (csvLine[i + 1] == "?") // be aware that first idx is for class value
                            {
                                featureRow[i] = float.NaN;
                                missingRow[i] = (byte)1;
                            }
                            else
                            {
                                featureRow[i] = float.Parse(csvLine[i + 1]);
                                missingRow[i] = (byte)0;
                            }
                        }
                        rowIdx++;
                    }
                }
            }
        }

        // Generate random idxs which the range is from start to (start + count), for numIndexes
        public static int[] CreateRandomIdxs(int start, int indexTotalNum, int numRndIndexes)
        {
            if(numRndIndexes > indexTotalNum) throw new Exception("numIndexes has to be smaller than count");
            var rnd = new Random();
            var candidateIdxs = Enumerable.Range(start, indexTotalNum).ToList();
            var randomIdxs = new int[numRndIndexes];
            for(int i = 0; i < numRndIndexes; i++) { 
                var rndIdx = rnd.Next(candidateIdxs.Count());
                randomIdxs[i] = candidateIdxs[rndIdx];
                candidateIdxs.RemoveAt(rndIdx);
            }
            return randomIdxs.OrderBy(idx => idx).ToArray();
        }

        // Random sample attributes of given feature data 
        public static Tuple<FeatureData, int[]> RandomSampleAttributes(FeatureData featureData, int numSample)
        {
            var features = featureData.Features;
            var featureDim = features.Cols;
            var numFeatures = features.Rows;
            var rndIdxs = CreateRandomIdxs(0, featureDim, numSample);
            var missings = featureData.MissingValues;
            var rndFeatures =  new CvMat(numFeatures, numSample, features.ElemType);
            var rndMissings = new CvMat(numFeatures, numSample, missings.ElemType);
            unsafe
            {
                for(int attrIdx = 0; attrIdx < numSample; attrIdx++)
                {
                    var rndIdx = rndIdxs[attrIdx];
                    // featureDimension * rowIdx will beyond 32bit size if you are working with large size data
                    for (long rowIdx = 0; rowIdx < numFeatures; rowIdx++)
                    {
                        float* featureRow = features.DataSingle + (featureDim * rowIdx);
                        float* rsFeatureRow = rndFeatures.DataSingle + (numSample * rowIdx);
                        rsFeatureRow[attrIdx] = featureRow[rndIdx]; // copy rndIdx-th col  value 
                        byte* missingRow = missings.DataByte + (featureDim * rowIdx);
                        byte* rsMissingRow = rndMissings.DataByte + (numSample * rowIdx);
                        rsMissingRow[attrIdx] = missingRow[rndIdx];
                    }
                }
            }
            var randomSampledFeatureData = new FeatureData();
            randomSampledFeatureData.Features = rndFeatures;
            randomSampledFeatureData.MissingValues = rndMissings;
            randomSampledFeatureData.Classes = featureData.Classes.Clone();
            randomSampledFeatureData.ClassIdxDict = new Dictionary<string, int>();
            foreach (var kv in featureData.ClassIdxDict) randomSampledFeatureData.ClassIdxDict.Add(kv.Key, kv.Value);
            return new Tuple<FeatureData, int[]>(randomSampledFeatureData, rndIdxs);
        }
    
        public static Tuple<int, int>[] ChooseRandomPairs(int indexTotalNum, int numPairs){
            var pairHash = new HashSet<Tuple<int, int>>();
            var rnd = new Random();
            for (int i = 0; i < numPairs; i++)
            {
                var candidate1 = rnd.Next(0, indexTotalNum - 1);
                var candidate2 = rnd.Next(0, indexTotalNum - 1);
                // in order not to add same pairs in to hash
                var pair = candidate1 <= candidate2 ? new Tuple<int, int>(candidate1, candidate2) : new Tuple<int, int>(candidate2, candidate1);
                while (candidate1 == candidate2 || pairHash.Contains(pair))
                {
                    candidate2 = rnd.Next(0, indexTotalNum - 1);
                    pair = candidate1 <= candidate2 ? new Tuple<int, int>(candidate1, candidate2) : new Tuple<int, int>(candidate2, candidate1);
                }
                pairHash.Add(pair);
            }
            return pairHash.OrderBy(tpl => tpl.Item1).ToArray();
        }
    }
}
