﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace OpenCvUtility.Image
{
    /// <summary>
    /// Hsv (HSB) Color Class
    /// </summary>
    public class Hsv
    {
        private float h;
        // Hue
        public float H { get { return this.h; } }

        private float s;
        // Saturation
        public float S { get { return this.s; } }

        private float v;
        // Value, Brightness
        public float V { get { return this.v; } }

        const ushort CONVERSION_MAX_VAL = 9000; // You can change this value up to 36000
        const float MAP_DEGREE = 360f / CONVERSION_MAX_VAL; // by multiplying value to this you will get degree of Hue

        public Hsv(float hue, float saturation, float brightness)
        {
            if (hue < 0f || 360f <= hue)
            {
                throw new ArgumentException(
                    "hue has to be between 0 to 360", "hue");
            }
            if (saturation < 0f || 1f < saturation)
            {
                throw new ArgumentException(
                    "saturation has to be between 0 to 1", "saturation");
            }
            if (brightness < 0f || 1f < brightness)
            {
                throw new ArgumentException(
                    "brightness  has to be between 0 to 1", "brightness");
            }
            this.h = hue;
            this.s = saturation;
            this.v = brightness;
        }

        /// <summary>
        /// Create hsv from rgb color
        /// </summary>
        /// <param name="rgb">Color</param>
        /// <returns>HsvColor</returns>
        public static Hsv FromRgb(Color rgb)
        {
            float r = (float)rgb.R / 255f;
            float g = (float)rgb.G / 255f;
            float b = (float)rgb.B / 255f;

            float max = Math.Max(r, Math.Max(g, b));
            float min = Math.Min(r, Math.Min(g, b));

            float brightness = max;

            float hue, saturation;
            if (max == min)
            {
                //undefined
                hue = 0f;
                saturation = 0f;
            }
            else
            {
                float c = max - min;

                if (max == r)
                {
                    hue = (g - b) / c;
                }
                else if (max == g)
                {
                    hue = (b - r) / c + 2f;
                }
                else
                {
                    hue = (r - g) / c + 4f;
                }
                hue *= 60f;
                if (hue < 0f)
                {
                    hue += 360f;
                }
                saturation = c / max;
            }
            return new Hsv(hue, saturation, brightness);
        }


        public Color ToRgb()
        {
            return Hsv.ToRgb(this);
        }

        /// <summary>
        /// Convert hsv to rgb color
        /// </summary>
        /// <param name="hsv">HsvColor</param>
        /// <returns>Color</returns>
        public static Color ToRgb(Hsv hsv)
        {
            float v = hsv.V;
            float s = hsv.S;

            float r, g, b;
            if (s == 0)
            {
                r = v; g = v; b = v;
            }
            else
            {
                float h = hsv.H / 60f;
                int i = (int)Math.Floor(h);
                float f = h - i;
                float p = v * (1f - s);
                float q;
                if (i % 2 == 0)
                {
                    q = v * (1f - (1f - f) * s);
                }
                else
                {
                    q = v * (1f - f * s);
                }

                switch (i)
                {
                    case 0: r = v; g = q; b = p; break;
                    case 1: r = q; g = v; b = p; break;
                    case 2: r = p; g = v; b = q; break;
                    case 3: r = p; g = q; b = v; break;
                    case 4: r = q; g = p; b = v; break;
                    case 5: r = v; g = p; b = q; break;
                    default: throw new ArgumentException("hue value is invalid", "hsv");
                }
            }
            return Color.FromArgb(0, (byte)Math.Round(r * 255f), (byte)Math.Round(g * 255f), (byte)Math.Round(b * 255f));
        }

        public string ToHsvString()
        {
            return string.Join(",", new string[] { H.ToString("F1"), S.ToString("F1"), V.ToString("F1") });
        }

        public static Hsv ValueToHsv(ushort value)
        {
            if (value < 0 || CONVERSION_MAX_VAL < value) new ArgumentOutOfRangeException("value has to be between 0 to " + CONVERSION_MAX_VAL);
            var mapped_h = value * MAP_DEGREE;
            var h = (ushort)(mapped_h * 2) / 2.0f; // H interval is 0.5
            var s = (float)(1 - 0.01 * (ushort)((mapped_h - h) / MAP_DEGREE)); // S interval is 0.01
            return new Hsv(h, s, 1);
        }
        public static Color ValueToRgb(ushort value)
        {
            return ValueToHsv(value).ToRgb();
        }

        public static ushort HsvToValue(Hsv hsv)
        {
            return (ushort)(Math.Ceiling(hsv.H / MAP_DEGREE) + (1 - hsv.S) * 100);
        }
        public static ushort RgbToValue(Color rgb)
        {
            return HsvToValue(Hsv.FromRgb(rgb));
        }
    }
}
