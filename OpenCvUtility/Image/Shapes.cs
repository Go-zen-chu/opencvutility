﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OpenCvUtility.Image
{
    public static class Shapes
    {
        // Extract src with rect and fill the outside of rect with black.
        public static void ExtractRect(this IplImage src, IplImage dst, Rect roi)
        {
            // selectedRect以外をマスキング
            using (var rectMaskImg = ImageUtil.CreateEmptyImage(src))
            {
                // Set thickness to -1 to fill the rect
                Cv.Rectangle(rectMaskImg, roi.ToCvRect(), CvScalar.ScalarAll(255), -1);
                Cv.And(src, rectMaskImg, dst);
            }
        }

        // Extract src with rect and fill the outside of rect with white.
        public static void ExtractRectWhite(this IplImage src, IplImage dst, Rect roi)
        {
            // selectedRect以外をマスキング
            using (var rectMaskImg = ImageUtil.CreateEmptyImage(src))
            {
                // Set thickness to -1 to fill the rect
                Cv.Rectangle(rectMaskImg, roi.ToCvRect(), CvScalar.ScalarAll(255), -1);
                // extract src with rect
                Cv.And(src, rectMaskImg, dst);
                // add white inverted mask to dst
                using (var rectInvertedMask = ImageUtil.InvertImage(rectMaskImg))
                {
                    Cv.Or(dst, rectInvertedMask, dst);
                }
            }
        }

        public static List<IplImage> ExtractRects_B(IplImage src, IEnumerable<Rect> rects)
        {
            var rectExtractedIpls = new List<IplImage>();
            using (var rectMaskImg = Cv.CreateImage(Cv.GetSize(src), src.Depth, src.NChannels))
            {
                foreach (var roi in rects)
                {
                    Cv.Zero(rectMaskImg);
                    // Set thickness to -1 to fill the rect
                    Cv.Rectangle(rectMaskImg, roi.ToCvRect(), CvScalar.ScalarAll(255), -1);
                    var rectExtractedIpl = ImageUtil.CreateEmptyImage(src);
                    Cv.And(src, rectMaskImg, rectExtractedIpl);
                    rectExtractedIpls.Add(rectExtractedIpl);
                }
            }
            foreach (var rect in rects)
            {
                // fill src with specified rectangles
                Cv.Rectangle(src, new CvRect((int)rect.Left, (int)rect.Top, (int)rect.Width, (int)rect.Height), CvScalar.ScalarAll(0), -1);
            }
            rectExtractedIpls.Add(src);
            return rectExtractedIpls;
        }

        public static Point[] CreateRotatedRectPoints(Point p0, Point p1, int width)
        {
            var heightVector = (p0 - p1);
            var widthVector = new Vector(heightVector.Y, -heightVector.X); // dot product will be Zero 
            widthVector.Normalize();
            var halfWidthVector = widthVector * width / 2;
            return new Point[] { p0 + halfWidthVector, p0 - halfWidthVector, p1 - halfWidthVector, p1 + halfWidthVector };
        }
        public static CvPoint[] CreateRotatedRectPoints(CvPoint p0, CvPoint p1, int width)
        {
            return CreateRotatedRectPoints(new Point(p0.X, p0.Y), new Point(p1.X, p1.Y), width).Select(p => p.ToCvPoint()).ToArray();
        }

        public static void ExtractPolygon<T>(T src, T dst, CvPoint[] polygon) where T: CvArr, ICloneable, IDisposable
        {
            using (var polyMask = src.Clone() as T)
            using(var cloneSrc = src.Clone() as T) // for when src == dst
            {
                Cv.Zero(polyMask); // turn into black
                Cv.FillConvexPoly(polyMask, polygon, CvScalar.ScalarAll(255));
                dst.Zero();
                cloneSrc.And(polyMask, dst);
            }
        }

        // _B for Bang methods
        public static List<IplImage> ExtractPolygons_B(IplImage src, IEnumerable<CvPoint[]> polygons)
        {
            var polyExtractedIpls = new List<IplImage>();
            using (var polyMaskImg = Cv.CreateImage(src.Size, src.Depth, src.NChannels))
            {
                foreach (var polygon in polygons)
                {
                    Cv.Zero(polyMaskImg);
                    // Set thickness to -1 to fill the rect
                    Cv.FillConvexPoly(polyMaskImg, polygon, CvScalar.ScalarAll(255));
                    var polyExtractedIpl = Cv.CreateImage(src.Size, src.Depth, src.NChannels);
                    polyExtractedIpl.Zero();
                    Cv.And(src, polyMaskImg, polyExtractedIpl);
                    polyExtractedIpls.Add(polyExtractedIpl);
                }
            }
            foreach (var polygon in polygons)
            {
                Cv.FillConvexPoly(src, polygon, CvScalar.ScalarAll(0));
            }
            polyExtractedIpls.Add(src);
            return polyExtractedIpls;
        }

        public static void DetectEllipse(this IplImage binSrc, IplImage binDst, int minContourPointNum = 60)
        {
            using (var storage = new CvMemStorage())
            {
                CvSeq<CvPoint> contours;
                Cv.FindContours(binSrc, storage, out contours, CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
                //Cv.DrawContours(ellipseDectectedIpl, contours, new CvScalar(0, 0, 255), new CvScalar(0, 255, 0), 3);
                while (contours != null)
                {
                    // contours.Totalは輪郭を構築する座標の数で、多いほど輪郭の周囲長が長い
                    if (contours.Total > minContourPointNum)
                    {
                        var ellipse = contours.FitEllipse2();
                        // 抽出された楕円の傾きと描画時の傾きが異なるため、調整
                        ellipse.Angle = 90 - ellipse.Angle;
                        Cv.EllipseBox(binDst, ellipse, new CvScalar(255), -1);
                    }
                    contours = contours.HNext;
                }
            }
        }

        public static List<Tuple<Point, float>> DetectCircles(IplImage src, int minRadius = 30, int maxRadius = 200)
        {
            var circleList = new List<Tuple<Point, float>>();
            Func<IplImage, List<Tuple<Point, float>>> hough = (IplImage gray) =>
            {
                using (var storage = new CvMemStorage())
                {
                    var circles = Cv.HoughCircles(gray, storage, HoughCirclesMethod.Gradient, 2, 40, 200, 45, minRadius, maxRadius);
                    foreach (var circleNullable in circles)
                    {
                        if(circleNullable != null){
                            var circle = circleNullable.Value;
                            circleList.Add(new Tuple<Point, float>(new Point(circle.Center.X, circle.Center.Y), circle.Radius));
                        }
                    }
                    return circleList;
                }
            };
            switch (src.NChannels)
            {
                case 1: return hough(src);
                case 3:
                    {
                        using (var gray = new IplImage(src.Size, BitDepth.U8, 1))
                        {
                            src.CvtColor(gray, ColorConversion.BgrToGray);
                            return hough(gray);
                        }
                    }
                default: throw new Exception("Unsupported src type");
            }
        }

        public static List<CvPoint[]> DetectPolygon(IplImage src, int numEdge, double minArea = 30, double maxArea = 300)
        {
            Func<IplImage, List<CvPoint[]>> detectPoly = (IplImage gray) =>
            {
                using (var storage = new CvMemStorage())
                {
                    CvSeq<CvPoint> contours;
                    Cv.FindContours(gray, storage, out contours, CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
                    var polyList = new List<CvPoint[]>();
                    while (contours != null)
                    {
                        var contourArea = -Cv.ContourArea(contours); // Maybe a bug but you need to multiply minus to get right contour area
                        if (minArea < contourArea && contourArea < maxArea)
                        {
                            // parameter describe how hard you approximate
                            var apprxPoly = Cv.ApproxPoly(contours, CvContour.SizeOf, storage, ApproxPolyMethod.DP, 30.0);
                            //TODO need to check the size 
                            if (apprxPoly.ElemSize == numEdge)
                            {
                                polyList.Add(apprxPoly.Select(p => p.Value).ToArray());
                            }
                        }
                        contours = contours.HNext;
                    }
                    return polyList;
                }
            };
            switch (src.NChannels)
            {
                case 1: return detectPoly(src);
                case 3:
                    {
                        using (var gray = new IplImage(src.Size, BitDepth.U8, 1))
                        {
                            src.CvtColor(gray, ColorConversion.BgrToGray);
                            return detectPoly(gray);
                        }
                    }
                default: throw new Exception("Unsupported src type");
            }
        }

        /// <summary>
        /// returns largest contour, the area of the contour, center of largest contour 
        /// </summary>
        /// <param name="binSrc"></param>
        /// <returns></returns>
        public static Tuple<CvPoint[], double, CvPoint> GetLargestContour_B(CvArr binSrc, double minimumArea = 0, Action<CvSeq<CvPoint>> actionForMaxSizeContour = null)
        {
            using (var storage = new CvMemStorage())
            {
                CvSeq<CvPoint> contours = null;
                // WARNING * binSrc is overwritten by using FindContours method.
                Cv.FindContours(binSrc, storage, out contours, CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
                CvSeq<CvPoint> maxSizedContour = null;
                double contourMaxArea = minimumArea;
                while (contours != null)
                {
                    var contourArea = -Cv.ContourArea(contours); // Maybe a bug but you need to multiply minus to get right contour area
                    if (contourArea > contourMaxArea)
                    {
                        contourMaxArea = contourArea;
                        maxSizedContour = contours;
                    }
                    contours = contours.HNext;
                }
                if (actionForMaxSizeContour != null && maxSizedContour != null) actionForMaxSizeContour(maxSizedContour);
                Tuple<CvPoint[], double, CvPoint> largestContourTpl = null;
                if(maxSizedContour != null){
                    CvMoments moment = null;
                    Cv.Moments(maxSizedContour, out moment, false);
                    largestContourTpl = new Tuple<CvPoint[],double,CvPoint>(
                        maxSizedContour.Select(pt => pt.Value).ToArray(), contourMaxArea, new CvPoint((int)(moment.M10 / moment.M00),(int)(moment.M01 / moment.M00)));
                }
                binSrc.Dispose();
                return largestContourTpl;
            }
        }

        // Fill largest contours in the binary image with white(255)
        public static void FillLargestContour<T>(this T binSrc, T binDst, double minimumArea = 0) where T: CvArr, ICloneable, IDisposable
        {
            GetLargestContour_B(binSrc.Clone() as T, minimumArea, (CvSeq<CvPoint> maxSizeContour) =>
            {
                binDst.Zero(); // if binSrc and binDst is same, make sure binDst has to be cleared to draw the largest contour
                binDst.DrawContours(maxSizeContour, new CvScalar(255), new CvScalar(0), 0, Cv.FILLED, LineType.AntiAlias);
            });
        }
        
        // Fill certain sized contours in the binary image with white(255)
        public static void FillContours(this IplImage binSrc, IplImage binDst, int minContourArea = 40, int maxContourArea = 1000)
        {
            using (var storage = new CvMemStorage())
            {
                CvSeq<CvPoint> contours;
                Cv.FindContours(binSrc, storage, out contours, CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
                var contoursList = new List<CvSeq<CvPoint>>();
                while (contours != null)
                {
                    var contourArea = -Cv.ContourArea(contours); // Maybe a bug but you need to multiply minus to get right contour area
                    if (minContourArea < contourArea && contourArea < maxContourArea)
                    {
                        contoursList.Add(contours);
                    }
                    contours = contours.HNext;
                }
                // contoursの中身をfillする(outsideの色で塗りつぶされることに注意, holecolorは無視される)
                if (contoursList.Count > 0)
                    contoursList.ForEach(c => binDst.DrawContours(c, new CvScalar(255), new CvScalar(0), 0, Cv.FILLED, LineType.AntiAlias));
            }
        }

        public static List<CvRect> FindContoursBoundingRect(IplImage binSrc, int contourAreaMin, int contourAreaMax)
        {
            using (var storage = new CvMemStorage())
            {
                CvSeq<CvPoint> contours;
                Cv.FindContours(binSrc, storage, out contours, CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
                var contoursList = new List<Tuple<CvSeq<CvPoint>, double>>();
                while (contours != null)
                {
                    var contourArea = -Cv.ContourArea(contours); // Maybe a bug but you need to multiply minus to get right contour area
                    if (contourAreaMin <= contourArea && contourArea <= contourAreaMax)
                    {
                        contoursList.Add(new Tuple<CvSeq<CvPoint>, double>(contours, contourArea));
                    }
                    contours = contours.HNext;
                }
                var rectList = new List<CvRect>();
                if (contoursList.Count > 0)
                {
                    foreach (var tpl in contoursList.OrderByDescending(tpl => tpl.Item2)) // order by contours area
                    {
                        var boundingRect = Cv.BoundingRect(tpl.Item1);
                        if (rectList.All(br => br.Contains(boundingRect) == false)) rectList.Add(boundingRect); // if bounding rect is not contained in all other rects
                    }
                }
                return rectList;
            }
        }
    }
}
