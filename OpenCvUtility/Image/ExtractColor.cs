﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OpenCvUtility.Image
{
    public static class ExtractColor
    {
        /// <summary>
        /// Extract color by specified hsv range
        /// </summary>
        /// <param name="src"></param>
        /// <param name="chMinArray"></param>
        /// <param name="chMaxArray"></param>
        /// <param name="exceptSingle">Flag which extract except specified color</param>
        /// <returns></returns>
        public static IplImage ExtractSingleColor(IplImage src, double[] chMinArray, double[] chMaxArray, bool exceptSingle = false)
        {
            var dstImg = Cv.CreateImage(Cv.GetSize(src), src.Depth, src.NChannels);
            Cv.Zero(dstImg);
            using (var maskImg = CreateHSVFilteringMask(src, chMinArray, chMaxArray))
            {
                if (exceptSingle) Cv.Not(maskImg, maskImg); // invert mask image so that it mask single color
                Cv.Copy(src, dstImg, maskImg);
            }
            return dstImg;
        }
        
        public static IplImage ExtractSingleColor(IplImage src, double h, double s, double v, bool exceptSingle = false)
        {
            // When v is small, the range of h value has to be larger.
            Func<double, double> hrange = vVal => (-40 * vVal + 50);
            return ExtractSingleColor(src, new double[] { h - hrange(v), s - 0.1, v - 0.2 }, new double[] { h + hrange(v), s + 0.1, v + 0.2 }, exceptSingle);
        }

        // returns mask image which filters hsvImg by argument ranges
        public static IplImage CreateHSVFilteringMask(IplImage src, double[] chMinArray, double[] chMaxArray)
        {
            if (chMinArray[0] >= chMaxArray[0] || chMinArray[1] >= chMaxArray[1] || chMinArray[2] >= chMaxArray[2])
                throw new ArgumentException("Problem with Channel Range");
            var maskImg = Cv.CreateImage(Cv.GetSize(src), src.Depth, 1);
            Cv.Zero(maskImg);
            using (var hsvImg = Cv.CreateImage(Cv.GetSize(src), src.Depth, src.NChannels))
            {
                // convert to hsv data
                Cv.CvtColor(src, hsvImg, ColorConversion.BgrToHsv);
                // because lut is 8 bit, convert channel values which makes color-resolution bit worse
                var chIntMinArray = new int[] { (int)(chMinArray[0] / 2.0), (int)(chMinArray[1] * 255), (int)(chMinArray[2] * 255) };
                var chIntMaxArray = new int[] { (int)(chMaxArray[0] / 2.0), (int)(chMaxArray[1] * 255), (int)(chMaxArray[2] * 255) };
                using (var lut = Cv.CreateMat(256, 1, MatrixType.U8C3))
                {
                    var values = new int[3];
                    for (int i = 0; i < 256; i++)
                    {
                        for (int channelIdx = 0; channelIdx < 3; channelIdx++)
                            values[channelIdx] = ((chIntMinArray[channelIdx] <= i) && (i <= chIntMaxArray[channelIdx])) ? 255 : 0;
                        Cv.Set1D(lut, i, new CvScalar(values[0], values[1], values[2]));
                    }
                    Cv.LUT(hsvImg, hsvImg, lut);
                }
                using (var ch1_img = Cv.CreateImage(Cv.GetSize(hsvImg), hsvImg.Depth, 1))
                using (var ch2_img = Cv.CreateImage(Cv.GetSize(hsvImg), hsvImg.Depth, 1))
                using (var ch3_img = Cv.CreateImage(Cv.GetSize(hsvImg), hsvImg.Depth, 1))
                {
                    Cv.Split(hsvImg, ch1_img, ch2_img, ch3_img, null);
                    Cv.And(ch1_img, ch2_img, maskImg);
                    Cv.And(maskImg, ch3_img, maskImg);
                }
            }
            return maskImg;
        }
    }
}
