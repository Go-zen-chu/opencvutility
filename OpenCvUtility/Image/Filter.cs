﻿using OpenCvSharp;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OpenCvUtility.Image
{
    public static class Filter
    {
        public static void Morphology<T>(T src, T dst, int kernelSize = 3, MorphologyOperation operation = MorphologyOperation.Open) where T: CvArr, ICloneable, IDisposable
        {
            if (kernelSize % 2 != 1) throw new Exception("kernelSize has to be odd number");
            using (var tmpImg = src.Clone() as T)
            {
                Cv.Zero(tmpImg);
                // kernelsizeでモルフォロジーの強度が決まる. kernel size has to be odd number
                var element = new IplConvKernel(kernelSize, kernelSize, kernelSize / 2, kernelSize / 2, ElementShape.Ellipse);
                // tmpImgはgradient, tophat, balckhatで必要になるらしい
                Cv.MorphologyEx(src, dst, tmpImg, element, operation);
            }
        }

        public static IplImage GaussianFilter(IplImage src, int blurStrength)
        {
            var dst = ImageUtil.CreateEmptyImage(src);
            Cv.Smooth(src, dst, SmoothType.Gaussian, blurStrength, blurStrength);
            return dst;
        }

    }
}
