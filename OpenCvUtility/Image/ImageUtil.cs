﻿using OpenCvSharp;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OpenCvUtility.Image
{
    public static class ImageUtil
    {
        //Create image filled with solid color
        public static IplImage CreateSolidColorImage(CvSize size, Color color)
        {
            var solidColorImg = Cv.CreateImage(size, BitDepth.U8, 3);
            solidColorImg.Zero();
            Cv.Rectangle(solidColorImg, new CvPoint(0, 0), new CvPoint(size.Width, size.Height), new CvScalar(color.B, color.G, color.R), -1);
            return solidColorImg;
        }

        /// <summary>
        /// Add all ipl images and get result. Elements of IplImages will be disposed.
        /// </summary>
        /// <param name="iplImages"></param>
        /// <returns></returns>
        public static IplImage ConcanateIplImages_B(IEnumerable<IplImage> iplImages)
        {
            return concanate_B(iplImages, Cv.Add);
        }
        public static IplImage ConcanateBinaryImages_B(IEnumerable<IplImage> binIplImages)
        {
            return concanate_B(binIplImages, Cv.Or);
        }
        private static IplImage concanate_B(IEnumerable<IplImage> iplImages, Action<IplImage, IplImage, IplImage> func)
        {
            var firstImage = iplImages.First();
            var dstImg = Cv.CreateImage(Cv.GetSize(firstImage), firstImage.Depth, firstImage.NChannels);
            Cv.Zero(dstImg);
            foreach (var iplImg in iplImages)
            {
                if (iplImg == null) continue;
                func(dstImg, iplImg, dstImg);
                iplImg.Dispose();// You need to dispose carefully otherwise you'll get Access Violation Error.
            }
            return dstImg;
        }

        /// <summary>
        /// Get the color of pixel at given point
        /// </summary>
        /// <param name="src"></param>
        /// <param name="pickedPoint"></param>
        /// <returns></returns>
        public static Color GetPickedColor(WriteableBitmap src, Point pickedPoint)
        {
            var cropppedBitmap = new CroppedBitmap(src, new Int32Rect((int)pickedPoint.X, (int)pickedPoint.Y, 1, 1));
            var pixelColor = new byte[4];
            cropppedBitmap.CopyPixels(pixelColor, 4, 0);
            return Color.FromRgb(pixelColor[2], pixelColor[1], pixelColor[0]);
        }

        // Average the color of sample points
        public static Color GetAverageColor(IplImage src, IEnumerable<CvPoint> samplePoints)
        {
            uint r = 0, g = 0, b = 0;
            var srcIntPtr = src.ImageData;
            foreach (var point in samplePoints)
            {
                var imgPixelIdx = point.Y * src.WidthStep + point.X * src.NChannels;
                b += Marshal.ReadByte(srcIntPtr, imgPixelIdx);
                g += Marshal.ReadByte(srcIntPtr, imgPixelIdx + 1);
                r += Marshal.ReadByte(srcIntPtr, imgPixelIdx + 2);
            }
            var sampleNum = (double)samplePoints.Count();
            return Color.FromRgb((byte)(r / sampleNum), (byte)(g / sampleNum), (byte)(b / sampleNum));
        }


        // Get Color using k means
        public static Color GetTopKmeansColor(IplImage src, IEnumerable<CvPoint> samplePoints, int maxClusterNum = 4)
        {
            if (src.NChannels != 3) throw new Exception("deskImg has to be 3 channels");
            var sampleNum = samplePoints.Count();
            var channelIdxs = Enumerable.Range(0, 3);

            using (var samplePointPixels = Cv.CreateMat(sampleNum, 1, MatrixType.F32C3))
            using (var clusters = Cv.CreateMat(sampleNum, 1, MatrixType.S32C1))
            {
                samplePointPixels.Zero();
                clusters.Zero();
                // 色の書き込み
                unsafe
                {
                    float* pSample = (float*)samplePointPixels.Data; // 参照点の色データへのポインタ
                    byte* pImg = (byte*)src.ImageData;   // 画像の画素へのポインタ
                    int sampleIdx = 0;
                    foreach (var point in samplePoints)
                    {
                        var imgPixelIdx = point.Y * src.WidthStep + point.X * src.NChannels;
                        // read bgr values and put into pixels array
                        foreach (var cIdx in channelIdxs)
                        {
                            pSample[sampleIdx] = pImg[imgPixelIdx + cIdx];
                            sampleIdx++;
                        }
                    }
                }
                // cluster the color of sample points
                Cv.KMeans2(samplePointPixels, maxClusterNum, clusters, new CvTermCriteria(10, 1.0));

                var maxMemberColorBGR = new float[3];
                // opencv のkmeansではcentroidが取得できない。そのため、同じクラスタの色を平均する
                using (CvMat color = Cv.CreateMat(maxClusterNum, 1, MatrixType.F32C3))
                using (CvMat count = Cv.CreateMat(maxClusterNum, 1, MatrixType.S32C1))
                {
                    color.Zero();
                    count.Zero();
                    unsafe
                    {
                        int* pClu = clusters.DataInt32;    // cluster の要素へのポインタ
                        int* pCnt = count.DataInt32;       // count の要素へのポインタ
                        float* pClr = color.DataSingle;    // color の要素へのポインタ
                        float* pPnt = samplePointPixels.DataSingle;   // points の要素へのポインタ
                        for (int pIdx = 0; pIdx < sampleNum; pIdx++)
                        {
                            int clsIdx = pClu[pIdx];     // クラスタのidx
                            int j = ++pCnt[clsIdx];   // クラスタに所属する数を増やす
                            // 色の平均化
                            pClr[clsIdx * 3 + 0] = pClr[clsIdx * 3 + 0] * (j - 1) / j + pPnt[pIdx * 3 + 0] / j;
                            pClr[clsIdx * 3 + 1] = pClr[clsIdx * 3 + 1] * (j - 1) / j + pPnt[pIdx * 3 + 1] / j;
                            pClr[clsIdx * 3 + 2] = pClr[clsIdx * 3 + 2] * (j - 1) / j + pPnt[pIdx * 3 + 2] / j;
                        }
                        // 所属する点が多いクラスタのidxを取得
                        int maxMemberNum = int.MinValue;
                        for (int clsIdx = 0; clsIdx < maxClusterNum; clsIdx++)
                        {
                            if (maxMemberNum < pCnt[clsIdx])
                            {
                                maxMemberNum = pCnt[clsIdx];
                                foreach (var cIdx in channelIdxs)
                                {
                                    maxMemberColorBGR[cIdx] = pClr[clsIdx * 3 + cIdx];
                                }
                            }
                        }
                    }
                }
                return Color.FromRgb((byte)maxMemberColorBGR[2], (byte)maxMemberColorBGR[1], (byte)maxMemberColorBGR[0]);
            }
        }

        public static double CalcMeanShift(IEnumerable<double> dataArray, double bandWidth, int maxIter = 100, double epsilon = 1.0E-6, double? initialVal = null)
        {
            var sortedArray = dataArray.OrderBy(val => val).ToArray();
            
            var mean = initialVal == null ? sortedArray[sortedArray.Length / 2] : initialVal.Value; // start from mode value in order to reduce iteration
            for (int iter = 0; iter < maxIter; iter++)
            {
                int divident = 0;
                double sum = 0;
                for (int i = 0; i < sortedArray.Length; i++)
                {
                    var val = sortedArray[i];
                    if (Math.Abs(val - mean) <= bandWidth)
                    {
                        sum += val; divident++;
                    }
                }
                double nextMean = sum / divident;
                if (Math.Abs(mean - nextMean) < epsilon) break;
                mean = nextMean;
            }
            return mean;
        }


        /// <summary>
        /// Crop image using region of interest
        /// </summary>
        /// <param name="src"></param>
        /// <param name="roi"></param>
        /// <returns></returns>
        public static IplImage CropImage(IplImage src, CvRect roi)
        {
            var dst = Cv.CreateImage(new CvSize(roi.Width, roi.Height), src.Depth, src.NChannels);
            dst.Zero();
            var srcIntPtr = src.ImageData;
            var dstIntPtr = dst.ImageData;
            var channelIdxs = Enumerable.Range(0, src.NChannels);
            Parallel.For(roi.Top, roi.Bottom + 1, row =>
            {
                var srcIdx = row * src.WidthStep + roi.Left * src.NChannels;
                var dstIdx = (row - roi.Top) * dst.WidthStep;
                for (int col = roi.Left; col < roi.Right + 1; col++)
                {
                    foreach (var cidx in channelIdxs)
                    {
                        Marshal.WriteByte(dstIntPtr, dstIdx, Marshal.ReadByte(srcIntPtr, srcIdx));
                        srcIdx++; dstIdx++;
                    }
                }
            });
            return dst;
        }

        // Invert given mask image
        public static IplImage InvertImage(IplImage src)
        {
            var dst = Cv.CreateImage(Cv.GetSize(src), src.Depth, src.NChannels);
            src.Not(dst);
            return dst;
        }

        /// <summary>
        /// Get the list of pixels which the color isn't black
        /// </summary>
        /// <param name="bgrSrc"></param>
        /// <returns></returns>
        public static ConcurrentBag<Tuple<int, int>> GetColoredPixels(IplImage bgrSrc)
        {
            if (bgrSrc.NChannels != 3) throw new Exception("NChannel of src has to be 3");
            var ptr = bgrSrc.ImageData;
            var coloredPixels = new ConcurrentBag<Tuple<int, int>>();
            Parallel.For(0, bgrSrc.Height, row =>
            {
                int byteIdx = bgrSrc.WidthStep * row;
                for (int col = 0; col < bgrSrc.Width; col++)
                {
                    byte b = Marshal.ReadByte(ptr, byteIdx++);    // B
                    byte g = Marshal.ReadByte(ptr, byteIdx++);    // G
                    byte r = Marshal.ReadByte(ptr, byteIdx++);    // R
                    if (((int)b) + g + r > 0) coloredPixels.Add(new Tuple<int, int>(col, row));
                }
            });
            return coloredPixels;
        }

        public static WriteableBitmap FlipBitmapLR(WriteableBitmap bmp)
        {
            //if (bmp.Format != PixelFormats.Bgra32) throw new Exception("Sorry this method doesn't support format except Bgra32");
            var width = bmp.PixelWidth;
            var height = bmp.PixelHeight;
            var srcBytes = new byte[bmp.BackBufferStride * height];
            bmp.CopyPixels(srcBytes, bmp.BackBufferStride, 0);

            var dstBytes = new byte[bmp.BackBufferStride * height];
            var stride = bmp.BackBufferStride;
            Parallel.For(0,height, row =>
            {
                var srcByteIdx = row * stride;
                for (int col = 0; col < width; col++)
                {
                    var dstByteIdx = (row + 1) * stride - 4 * (col + 1);
                    dstBytes[dstByteIdx + 0] = srcBytes[srcByteIdx++];
                    dstBytes[dstByteIdx + 1] = srcBytes[srcByteIdx++];
                    dstBytes[dstByteIdx + 2] = srcBytes[srcByteIdx++];
                    dstBytes[dstByteIdx + 3] = srcBytes[srcByteIdx++];
                }
            });

            var flippedBitmap = new WriteableBitmap(width, height, bmp.DpiX, bmp.DpiY, bmp.Format, bmp.Palette);
            flippedBitmap.WritePixels(new Int32Rect(0, 0, width, height), dstBytes, bmp.BackBufferStride, 0, 0);
            return flippedBitmap;
        }

        public static IplImage BgraToIplImage(WriteableBitmap bgraBitmap)
        {
            var width = bgraBitmap.PixelWidth; var height = bgraBitmap.PixelHeight;
            var dst = new IplImage(new CvSize(width, height), BitDepth.U8, 3);
            dst.Zero();

            bgraBitmap.Lock();
            unsafe
            {
                var srcData = bgraBitmap.BackBuffer;
                var dstData = dst.ImageData;
                Parallel.For(0, height, row =>
                {
                    int srcIdx = row * width * 4;
                    int dstIdx = row * width * 3;
                    for (int col = 0; col < width; col++)
                    {
                        Marshal.WriteByte(dstData, dstIdx++, Marshal.ReadByte(srcData, srcIdx++)); //b
                        Marshal.WriteByte(dstData, dstIdx++, Marshal.ReadByte(srcData, srcIdx++)); //g
                        Marshal.WriteByte(dstData, dstIdx++, Marshal.ReadByte(srcData, srcIdx++)); //r
                        srcIdx++;
                    }
                });
            }
            bgraBitmap.Unlock();
            return dst;
        }

        public static void ShowImage(IplImage img)
        {
            var windowName = Guid.NewGuid().ToString();
            Cv.ShowImage(windowName, img);
            Cv.WaitKey();
            Cv.DestroyWindow(windowName);
        }
        public static void ShowImages(IEnumerable<IplImage> images){
            var windowNameList = images.Select(ipl => Guid.NewGuid().ToString());
            for (int i = 0; i < images.Count(); i++)
                Cv.ShowImage(windowNameList.ElementAt(i), images.ElementAt(i));
            Cv.WaitKey();
            Cv.DestroyAllWindows();
        }

        public static IplImage CreateEmptyImage(this IplImage src)
        {
            var dst = Cv.CreateImage(Cv.GetSize(src), src.Depth, src.NChannels);
            dst.Zero();
            return dst;
        }

        /// <summary>
        /// Create a dictionary which stores color as a key and array of points which have a same color
        /// </summary>
        /// <param name="colorBitmap"></param>
        /// <returns></returns>
        public static Dictionary<Color, CvPoint[]> ClassifyPixelByColor(WriteableBitmap colorBitmap)
        {
            var colorDict = new ConcurrentDictionary<Color, ConcurrentBag<CvPoint>>();
            var bytesPerPixel = (colorBitmap.Format.BitsPerPixel + 7) / 8; // = 4 for argb, 3 for rgb
            if(bytesPerPixel != 3 && bytesPerPixel != 4) throw new NotImplementedException();
            int w = colorBitmap.PixelWidth; int h = colorBitmap.PixelHeight;
            var bgraPixels = new byte[bytesPerPixel * w * h];
            colorBitmap.CopyPixels(bgraPixels, bytesPerPixel * w, 0);
            Parallel.For(0, h, row =>
            {
                int idx = row * w * bytesPerPixel;
                for (int col = 0; col < w; col++)
                {
                    var color = bytesPerPixel == 3 ?
                        Color.FromRgb(bgraPixels[idx + 2], bgraPixels[idx + 1], bgraPixels[idx + 0]):
                        Color.FromArgb(bgraPixels[idx + 3], bgraPixels[idx + 2], bgraPixels[idx + 1], bgraPixels[idx + 0]);
                    if (colorDict.ContainsKey(color) == false) colorDict.TryAdd(color, new ConcurrentBag<CvPoint>());
                    colorDict[color].Add(new CvPoint(col, row));
                    idx += bytesPerPixel;
                }
            });
            var dict = new Dictionary<Color, CvPoint[]>();
            foreach (var kv in colorDict) dict.Add(kv.Key, kv.Value.ToArray());
            return dict;
        }

        public static double SquaredDistance(Point p1, Point p2)
        {
            return (p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y);
        }

        /// <summary>
        /// calculate cross product of p1p2 and p1p3 vectors
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public static double CrossProductPoints(Point p1, Point p2, Point p3)
        {
            return (p2.X - p1.X) * (p3.Y - p1.Y) - (p2.Y - p1.Y) * (p3.X - p1.X);
        }


        /// <summary>
        /// Round Point to CvPoint
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static CvPoint ToCvPoint(this Point point)
        {
            return new CvPoint((int)Math.Round(point.X), (int)Math.Round(point.Y));
        }
        /// <summary>
        /// Round Size to CvSize
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static CvSize ToCvSize(this Size size)
        {
            return new CvSize((int)Math.Round(size.Width), (int)Math.Round(size.Height));
        }
        /// <summary>
        /// Round Rect to CvRect
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static CvRect ToCvRect(this Rect rect)
        {
            return new CvRect(rect.TopLeft.ToCvPoint(), rect.Size.ToCvSize());
        }
    }
}
