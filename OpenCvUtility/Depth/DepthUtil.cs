﻿using OpenCvSharp;
using OpenCvUtility.Image;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace OpenCvUtility.Depth
{
    public static class DepthUtil
    {
        // Beacause handling short array you only need to use B and G channel
        public static byte[] DepthDataToByteArray(short[] depthData)
        {
            var byteArray = new byte[depthData.Length * 4];
            var byteArrayIdx = 0;
            short depth;
            for (int i = 0; i < depthData.Length; i++)
            {
                depth = depthData[i];
                if (depth < 0) depth = 0;
                byteArray[byteArrayIdx++] = (byte)(depth & 0x00ff); //b 
                byteArray[byteArrayIdx++] = (byte)((depth & 0xff00) >> 8); //g 
                byteArray[byteArrayIdx++] = 0; //r
                byteArray[byteArrayIdx++] = 255; //a for visibility
            }
            return byteArray;
        }
        public static WriteableBitmap ConvertDepthValToDepthImage(short[] depthData, int imgWidth, int imgHeight, int dpi = 96, PixelFormat? pixelFormat = null)
        {
            if (pixelFormat == null) pixelFormat = PixelFormats.Bgr32;
            var wb = new WriteableBitmap(imgWidth, imgHeight, dpi, dpi, pixelFormat.Value, null);
            wb.WritePixels(new Int32Rect(0, 0, imgWidth, imgHeight), DepthDataToByteArray(depthData), imgWidth * sizeof(int), 0);
            return wb;
        }

        static T[] convertDepthImageToDepthVal<T>(WriteableBitmap depthBitmap, Func<int, T> castFunc)
        {
            T[] depthData = null;
            int w = depthBitmap.PixelWidth; int h = depthBitmap.PixelHeight;
            var argbPixels = new int[w * h];
            depthData = new T[w * h];
            depthBitmap.CopyPixels(argbPixels, 4 * w, 0);
            Parallel.For(0, h, row =>
            {
                int idx = row * w;
                for (int col = 0; col < w; col++)
                {
                    depthData[idx] = castFunc(argbPixels[idx]);
                    idx++;
                }
            });
            return depthData;
        }
        /// <summary>
        /// Method for loading depth image and convert it to depth data
        /// </summary>
        /// <param name="depthImageDirPath"></param>
        /// <returns></returns>
        public static short[] ConvertDepthImageToShortDepthVal(WriteableBitmap depthBitmap)
        {
            return convertDepthImageToDepthVal<short>(depthBitmap, (pixel) => ((short)pixel));
        }
        public static ushort[] ConvertDepthImageToUShortDepthVal(WriteableBitmap depthBitmap)
        {
            return convertDepthImageToDepthVal<ushort>(depthBitmap, (pixel) => ((ushort)(pixel & 0x0000FFFF)));
        }

        // Fill image by white where the depth is larger than threshold
        public static void ExtractColorByDepthThreshold(IplImage src, IplImage dst, short[] depthData, short depthThresholdMax, short depthThresholdMin = 0, byte outlierFillByte = 255)
        {
            if (src.NChannels != dst.NChannels) throw new Exception("NChannels of src and dst have to be the same");
            if (src.Size != dst.Size) throw new Exception("Size of src and dst have to be the same");
            var srcIntPtr = src.ImageData;
            var dstIntPtr = dst.ImageData;
            int nChannels = src.NChannels;
            var channelIdxs = Enumerable.Range(0, nChannels);
            Parallel.For(0, src.Height, row =>
            {
                var depthDataIdx = row * src.Width;
                for (var col = 0; col < src.Width; col++)
                {
                    int imgOffset = src.WidthStep * row + nChannels * col; // rgb
                    // if color of pixel is same as outlierFillByte, ignore because its unnecessary
                    if (channelIdxs.All(cIdx => Marshal.ReadByte(srcIntPtr, imgOffset + cIdx) == outlierFillByte) == false)
                    {
                        if (depthData[depthDataIdx] <= depthThresholdMin || depthData[depthDataIdx] > depthThresholdMax) // depth value is larger than threshold
                        {
                            foreach (var cIdx in channelIdxs)
                                Marshal.WriteByte(dstIntPtr, imgOffset + cIdx, outlierFillByte);
                        }
                        else
                        {
                            foreach (var cIdx in channelIdxs)
                                Marshal.WriteByte(dstIntPtr, imgOffset + cIdx, Marshal.ReadByte(srcIntPtr, imgOffset + cIdx));
                        }
                    }
                    depthDataIdx += 1;
                }
            });
        }

        public static void ExtractByPlaneOffset(IplImage src, IplImage dst, short[] depthData, PlaneFinder.Plane plane, Rect roi, short fromPlaneMin = 15, short fromPlaneMax = 200, byte outlierFillByte = 0)
        {
            var srcIntPtr = src.ImageData;
            var dstIntPtr = dst.ImageData;
            var channelIdxs = Enumerable.Range(0, src.NChannels);
            Parallel.For(0, src.Height, row =>
            {
                var depthIdx = row * src.Width;
                var byteIdx = row * src.WidthStep;
                for (int col = 0; col < src.Width; col++)
                {
                    if (roi.Contains(col, row))
                    {
                        var depth = depthData[depthIdx];
                        var planeZ = plane.CalcZValue(col, row);
                        if ((planeZ - fromPlaneMax) < depth && depth < planeZ - fromPlaneMin)
                        {
                            foreach (var cidx in channelIdxs)
                            {
                                Marshal.WriteByte(dstIntPtr, byteIdx, Marshal.ReadByte(srcIntPtr, byteIdx));
                                byteIdx++;
                            }
                        }
                        else // outside of depth
                        {
                            foreach (var cidx in channelIdxs)
                            {
                                Marshal.WriteByte(dstIntPtr, byteIdx, outlierFillByte);
                                byteIdx++;
                            }
                        }
                    }
                    else // out of plane size
                    {
                        foreach (var cidx in channelIdxs)
                        {
                            Marshal.WriteByte(dstIntPtr, byteIdx, outlierFillByte);
                            byteIdx++;
                        }
                    }
                    depthIdx++;
                }
            });
        }

        // returns points which includes in plane offset. Be aware that returned array is randomized
        public static CvPoint[] GetPointsByPlaneOffset(short[] depthData, int width, int height, PlaneFinder.Plane plane, short fromPlaneMin = 15, short fromPlaneMax = 200)
        {
            var pointsBag = new ConcurrentBag<CvPoint>();
            Parallel.For(0, height, row =>
            {
                var depthIdx = row * width;
                for (int col = 0; col < width; col++)
                {
                    var depth = depthData[depthIdx];
                    var planeZ = plane.CalcZValue(col, row);
                    if ((planeZ - fromPlaneMax) < depth && depth < planeZ - fromPlaneMin)
                    {
                        pointsBag.Add(new CvPoint(col, row));
                    }
                    depthIdx++;
                }
            });
            return pointsBag.ToArray();
        }
        public static CvPoint[] GetPointsByDepthThreshold(short[] depthData, int width, int height, short depthThresholdMin = 1000, short depthThresholdMax = 1800)
        {
            var pointsBag = new ConcurrentBag<CvPoint>();
            Parallel.For(0, height, row =>
            {
                var depthIdx = row * width;
                for (int col = 0; col < width; col++)
                {
                    var depth = depthData[depthIdx];
                    if (depthThresholdMin <= depth && depth <= depthThresholdMax)
                    {
                        pointsBag.Add(new CvPoint(col, row));
                    }
                    depthIdx++;
                }
            });
            return pointsBag.ToArray();
        }

        /// <summary>
        /// Extract the src image from searchDepthMin to searchDepthMax by depthInterval and stops when the extracted area get larger than areaThreshold
        /// </summary>
        /// <param name="src"></param>
        /// <param name="depthData"></param>
        /// <param name="searchDepthMin"></param>
        /// <param name="searchDepthMax"></param>
        /// <param name="depthInterval"></param>
        /// <param name="areaThreshold"></param>
        /// <returns></returns>
        public static IplImage ExtractMaskUntilAreaThreshold(IplImage src, short[] depthData, short searchDepthMin = 500, short searchDepthMax = 2000, short depthInterval = 30, double areaThreshold = 1000)
        {
            var tmpImg = Cv.CreateImage(Cv.GetSize(src), src.Depth, src.NChannels);
            tmpImg.Zero();
            ExtractColorByDepthThreshold(src, tmpImg, depthData, searchDepthMax, searchDepthMin);
            var roughlyExtractedIpl = Cv.CreateImage(Cv.GetSize(src), BitDepth.U8, 1);
            roughlyExtractedIpl.Zero();
            tmpImg.CvtColor(roughlyExtractedIpl, ColorConversion.BgrToGray); // Convert to gray, nchannel: 3 -> 1
            tmpImg.Dispose();

            Cv.Threshold(roughlyExtractedIpl, roughlyExtractedIpl, 1, 255, ThresholdType.Binary); // Binarize mask image for inverting image
            IplImage depthThresMask = null;
            short workingDepth = (short)(searchDepthMin + depthInterval);
            while (searchDepthMax > workingDepth)
            {
                depthThresMask = Cv.CreateImage(Cv.GetSize(src), BitDepth.U8, 1);
                depthThresMask.Zero();
                ExtractColorByDepthThreshold(roughlyExtractedIpl, depthThresMask, depthData, workingDepth, searchDepthMin, outlierFillByte:0);
                var contourInfo =Shapes.GetLargestContour_B(depthThresMask); // depthThresMask will be disposed
                if(contourInfo != null){
                    var headArea = contourInfo.Item2;
                    if (headArea > areaThreshold) break;
                }
                workingDepth += depthInterval;
            }
            depthThresMask = Cv.CreateImage(Cv.GetSize(src), BitDepth.U8, 1);
            depthThresMask.Zero();
            // Because GetLargestContour_B is bang method you need to perform ExtractColorByDepthThreshold again to get proper headMaskIpl
            ExtractColorByDepthThreshold(roughlyExtractedIpl, depthThresMask, depthData, workingDepth, searchDepthMin, outlierFillByte: 0);
            roughlyExtractedIpl.Dispose();
            return depthThresMask;
        }

        public static CvPoint3D[] ExtractDepthDataByMat(short[] depthData, CvMat maskMat, short minDepth = 0, short maxDepth = short.MaxValue)
        {
            //if (initialCapacityOfList < 0) initialCapacityOfList = maskMat.Cols * maskMat.Rows / 10;
            var point3dBag = new ConcurrentBag<CvPoint3D>();
            Parallel.For(0, maskMat.Rows, row =>
            {
                var depthIdx = row * maskMat.Cols;
                for (int col = 0; col < maskMat.Cols; col++)
                {
                    var depth = depthData[depthIdx];
                    if (maskMat[row, col] > 0 && minDepth <= depth && depth <= maxDepth)
                    {
                        point3dBag.Add(new CvPoint3D(col, row, depth));
                    }
                    depthIdx++;
                }
            });
            return point3dBag.OrderBy(p3d => p3d.Y).ThenBy(p3d => p3d.X).ToArray();
        }
    
    }

    public struct CvPoint3D
    {
        public int X;
        public int Y;
        public int Z;

        public static CvPoint3D operator -(CvPoint3D pt)
        {
            return new CvPoint3D(-pt.X, -pt.Y, -pt.Z); 
        }
        public static CvPoint3D operator -(CvPoint3D p1, CvPoint3D p2)
        {
            return new CvPoint3D(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z); 
        }
        public static CvPoint3D operator +(CvPoint3D p1, CvPoint3D p2)
        {
            return new CvPoint3D(p1.X + p2.X, p1.Y + p2.Y, p1.Z + p2.Z); 
        }

        public CvPoint3D(int x, int y, int z)
        {
            this.X = x; this.Y = y; this.Z = z;
        }

    }
}
