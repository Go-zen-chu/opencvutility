﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenCvUtilityTest
{
    public static class TestUtil
    {
        public static string GetProjectPath()
        {
            var projectPath = Environment.CurrentDirectory;
            while (Path.GetFileName(projectPath) != "OpenCvUtilityTest")
                projectPath = Path.GetDirectoryName(projectPath);
            return projectPath;
        }

        public static string GetTestDataPath()
        {
            return Path.Combine(GetProjectPath(), "TestData");
        }

        public static string GetTmpDataPath()
        {
            return Path.Combine(GetProjectPath(), "tmp");
        }
    }
}
