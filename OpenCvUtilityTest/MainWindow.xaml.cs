﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using OpenCvUtility.Image;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenCvUtilityTest
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        WriteableBitmap srcBmp = null;
        IplImage src = null;

        public MainWindow()
        {
            InitializeComponent();
            if (File.Exists(Properties.Settings.Default.ImagePath) == false)
            {
                if (selectImagePath() == false) return;
            }
            initialize();
        }

        void initialize()
        {
            imagePathTextBox.Text = Properties.Settings.Default.ImagePath;
            srcBmp = new WriteableBitmap(new BitmapImage(new Uri(Properties.Settings.Default.ImagePath)));
            srcImage.Source = srcBmp;
            if (src != null) src.Dispose();
            src = srcBmp.ToIplImage();
        }

        bool selectImagePath()
        {
            var ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Title = "Choose image path";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Properties.Settings.Default.ImagePath = ofd.FileName;
                Properties.Settings.Default.Save();
                return true;
            }
            else
            {
                this.Close();
                return false;
            }
        }

        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            dstImage.Source = null;
        }

        private void flipButton_Click(object sender, RoutedEventArgs e)
        {
            dstImage.Source = ImageUtil.FlipBitmapLR(srcBmp);
        }

        private void classifyColorButton_Click(object sender, RoutedEventArgs e)
        {
            var colorDict = ImageUtil.ClassifyPixelByColor(srcBmp);
            foreach (var kv in colorDict)
            {
                Console.WriteLine(kv.Key + " num " + kv.Value.Length + " points " + string.Join(",", kv.Value.Take(20).Select(pt => "(" + pt.X + "," + pt.Y + ")")));
            }
        }

        private void fillLargestContourButton_Click(object sender, RoutedEventArgs e)
        {
            var binSrc = Cv.CreateImage(src.Size, src.Depth, 1);
            src.CvtColor(binSrc, ColorConversion.BgraToGray);
            Shapes.FillLargestContour(binSrc, binSrc);
            dstImage.Source = binSrc.ToWriteableBitmap();
            binSrc.Dispose();
        }

        private void setImagePathButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectImagePath() == false) return;
            initialize();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (src != null) src.Dispose();
        }
    }
}
