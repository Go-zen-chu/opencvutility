﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenCvUtility.ML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenCvUtilityTest.ML
{
    [TestClass]
    public class CustomRandomForestTest
    {
        [TestMethod]
        public  void Build_Test()
        {
            var tmpDir = TestUtil.GetTmpDataPath();
            Directory.CreateDirectory(tmpDir);
            var training = MLUtilTest.GetTestFeatureData();
            var cr = CustomRandomForest.Build(training, 4, 5, Path.Combine(tmpDir, "CRForest.model"));
            Assert.IsNotNull(cr);
        }

    }
}
