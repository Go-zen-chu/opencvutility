﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenCvUtility.ML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenCvUtilityTest.ML
{

    [TestClass]
    public class MLUtilTest
    {
        public static MLUtil.FeatureData GetTestFeatureData()
        {
            var testDataPath = TestUtil.GetTestDataPath();
            var classIdxDict = new Dictionary<string, int>() { { "class1", 0 }, { "class2", 1 }, { "class3", 2 } };
            return MLUtil.LoadFeatureDataFile(Path.Combine(testDataPath, "wine.csv"), 178, 13, classIdxDict);
        }

        [TestMethod]
        public void LoadFeatureDataFile_Test()
        {
            var featureData = GetTestFeatureData();
            featureData.Dispose();
        }

        [TestMethod]
        public void LoadFeatureDataFilesParallel_Test()
        {

        }
    }
}
